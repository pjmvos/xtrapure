from conf.config import DB_URL
from sqlalchemy import Column, Integer, Text, ForeignKey, Date, DateTime, create_engine, String, func, BigInteger, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship

#drop database pure;
#create database pure   DEFAULT CHARACTER SET utf8mb4  DEFAULT COLLATE utf8mb4_unicode_ci;

Base = declarative_base()
engine = create_engine(
    '%s?charset=utf8mb4' % DB_URL,
    isolation_level="READ UNCOMMITTED", pool_size=20, max_overflow=100
)
Session = sessionmaker(bind=engine)

class DoiPdf(Base):
    '''
    Table used for testing a full-text harvester. Not used for XtraPure
    '''
    __tablename__ = 'doi_pdf'
    doi = Column(String(255), primary_key=True)
    landing_page = Column(Text)
    pdf_url = Column(Text)
    found_date = Column(
        Date,
        default=func.date(func.now())
    )

class Stats(Base):
    '''
    Table for a few counters
    '''
    __tablename__ = 'stats'
    id = Column(Integer, primary_key=True)
    total = Column(Integer)
    with_pdf = Column(Integer) # research outputs with a pdf in Pure
    with_doi = Column(Integer) # research outputs with a doi in Pure
    with_scopus = Column(Integer) # research outputs with a scopus id in Pure
    oa = Column(Integer) # research outputs registered as open access in Pure
    with_up_is_oa = Column(Integer) # research outputs open access according to UnPaywall
    person_total = Column(Integer) # persons in Pure
    person_with_scopus = Column(Integer)
    person_with_orcid = Column(Integer)
    person_with_digital = Column(Integer)
    person_with_researcherid = Column(Integer)
    job_date = Column(
        Date,
        default=func.now()
    )

class Jobs(Base):
    '''
    Info about the daily jobs is stored here.
    '''
    __tablename__ = 'jobs'
    id = Column(Integer, primary_key=True)
    action = Column(String(255))
    first_id = Column(String(45))
    last_id = Column(String(45))
    changes = Column(Integer)
    db_changes = Column(Integer)
    deletes = Column(Integer)
    author_changes = Column(Integer)
    author_deletes = Column(Integer)
    start_total = Column(Integer)
    end_total = Column(Integer)
    duration = Column(BigInteger)
    job_date = Column(
        Date,
        default=func.now()
    )

class Author(Base):
    '''
    Persons in Pure, with their assosiated ids registered in Pure

    Note the relationship table
    '''
    __tablename__ = 'author'
    uuid = Column(String(255), primary_key=True)
    firstname = Column(String(255))
    lastname = Column(String(255))
    orcid = Column(String(255))
    scopusid = Column(String(255))
    digitalid = Column(String(255))
    researcherid = Column(String(255))
    isni = Column(String(255))
    deleted = Column(Boolean)
    created = Column(
        Date,
        default=func.date(func.now())
    )
    updated = Column(
        Date,
        onupdate=func.date(func.now())
    )
    publications = relationship(
        'Publication',
        secondary='author_has_publication'
    )

class Organization(Base):
    '''
    Organizations in Pure

    Note the relationship table
    '''
    __tablename__ = 'organization'
    uuid = Column(String(255), primary_key=True)
    name = Column(String(255))
    publications = relationship(
        'Publication',
        secondary='organization_has_publication'
    )


class Journal(Base):
    '''
    Some journal info, publication table links to this one
    '''
    __tablename__ = 'journal'
    uuid = Column(String(255), primary_key=True)
    title = Column(Text)
    issn = Column(String(45))


class ElectronicVersion(Base):
    '''
    files in Pure associated with research outputs, see the relationship table
    '''
    __tablename__ = 'electronicversion'
    id = Column(String(255), primary_key=True)
    type = Column(String(255))
    access_type = Column(String(255))
    version_type = Column(String(255))
    visible_on_portal_date = Column(String(255))
    file_creator = Column(String(255))
    file_created = Column(String(255))
    file_title = Column(Text)
    file_urls = Column(Text) # multivalued?
    file_total = Column(Integer)
    doi = Column(String(255))
    link = Column(Text)
    publications = relationship(
        'Publication',
        secondary='electronicversion_has_publication'
    )


class WosCheckDoi(Base):
    '''
    Test table Web of Science, unused in XtraPure
    '''
    __tablename__ = 'wos_doicheck'
    uuid = Column(String(255), primary_key=True)
    pure_doi = Column(String(255))
    wos_doi = Column(String(255))
    wos_ut = Column(String(255))
    pure_ut = Column(String(255))

class Wos(Base):
    '''
    Test table Web of Science, unused in XtraPure
    '''
    __tablename__ = 'wos'
    doi = Column(String(255))
    ut = Column(String(255), primary_key=True)
    pmid = Column(String(255))
    times_cited = Column(Integer)
    created = Column(
        Date,
        default=func.date(func.now())
    )
    updated = Column(
        Date,
        onupdate=func.date(func.now())
    )
    woss = relationship(
        'Publication',
        secondary='wos_has_publication'
    )



class ScopusAuthor(Base):
    '''
    Author information gathered from Scopus

    Note the relation to the scopus publication table
    '''
    __tablename__ = 'scopusauthor'
    auid = Column(String(255), primary_key=True)
    initials = Column(String(255))
    surname = Column(String(255))
    given_name = Column(String(255))
    indexed_name = Column(String(255))
    scopuss = relationship(
        'Scopus',
        secondary = 'scopusauthor_has_scopus'
    )

class ScopusAuthorHasAffiliation(Base):
    '''
    It's better to use ScopusAuthorAffiliations
    '''
    __tablename__ = 'scopusauthor_has_affiliation'
    afid = Column(String(255), primary_key=True)
    auid = Column(String(255), primary_key=True)

class ScopusAuthorAffiliations(Base):
    __tablename__ = 'scopusauthoraffiliations'
    eid = Column(String(255), primary_key=True)
    auid = Column(String(255), primary_key=True)
    afid = Column(String(255), primary_key=True)

class ScopusCorrespondence(Base):
    '''
    Persons found im Correspondence in Scopus
    '''
    __tablename__ = 'scopuscorrespondence'
    eid = Column(String(255), primary_key=True)
    initials = Column(String(255))
    surname = Column(String(255))
    indexed_name = Column(String(255))
    given_name = Column(String(255))
    country = Column(String(255))
    organization_last = Column(String(255))
    email = Column(String(255))

class Scopus(Base):
    '''
    Publication imfo from Scopus

    Note the relation to the Publication (Pure research outputs) table and the scopusauthor table
    '''
    __tablename__ = 'scopus'
    eid = Column(String(255), primary_key=True)
    doi = Column(String(255))
    pmid = Column(String(255))
    refartnum = Column(String(255))
    eissn = Column(String(255))
    issn = Column(String(255))
    pub_date = Column(Date)
    title = Column(Text)
    volume = Column(String(255))
    issue = Column(String(255))
    pages = Column(String(255))
    citedby_count = Column(Integer)
    found_on = Column(String(255))
    created = Column(
        Date,
        default=func.date(func.now())
    )
    updated = Column(
        Date,
        onupdate=func.date(func.now())
    )
    scopuss = relationship(
        'Publication',
        secondary='scopus_has_publication'
    )
    authors = relationship(
        ScopusAuthor,
        secondary='scopusauthor_has_scopus'
    )

class UnPaywall(Base):
    '''
    Publication info from unpaywall

    Note the relation to the Publication (Pure research outputs) table
    '''
    __tablename__= 'unpaywall'
    doi = Column(String(255), primary_key=True)
    best_oa_location = Column(Integer)
    data_standard = Column(Integer)
    genre = Column(String(255))
    is_oa = Column(Boolean)
    journal_is_in_doaj = Column(Boolean)
    journal_is_oa = Column(Boolean)
    journal_issns = Column(String(255))
    journal_name = Column(String(255))
    published_date = Column(String(45))
    publisher = Column(String(255))
    title = Column(Text)
    updated_oaidoi = Column(DateTime)
    year = Column(Integer)
    created = Column(
        Date,
        default=func.date(func.now())
    )
    updated = Column(
        Date,
        onupdate=func.date(func.now())
    )
    oaidois = relationship(
        'Publication',
        secondary='unpaywall_has_publication'
    )

class UnPaywall_Location(Base):
    '''
    every record in UnPaywall can have one or more locations (url).

    The unpaywall tables are linked on doi {no foreign key???}
    '''
    __tablename__ = 'unpaywall_location'
    id = Column(Integer, primary_key=True)
    pmh_id = Column(String(255))
    doi = Column(String(255))
    evidence = Column(String(255))
    host_type = Column(String(45))
    is_best = Column(Boolean)
    license = Column(String(255))
    updated = Column(DateTime)
    url = Column(Text)
    url_for_landing_page = Column(Text)
    url_for_pdf = Column(Text)
    version = Column(String(45))

class Publication(Base):
    '''
    This is the central table containing the Pure Research Output records
    '''
    __tablename__ = 'publication'
    uuid = Column(String(255), primary_key=True)
    doi = Column(String(255))
    pmid = Column(String(255))
    wos_accnum = Column(String(255))
    scopusid = Column(String(255))
    pub_date = Column(Date)
    epub_date = Column(Date)
    title = Column(Text)
    subtitle = Column(Text)
    volume = Column(String(255))
    issue = Column(String(255))
    article_number = Column(String(255))
    pages = Column(String(255))
    pubtype = Column(String(255))
    category = Column(String(255))
    peer_review = Column(String(255))
    oa_permission = Column(String(255))
    ISBN = Column(String(45))
    confidential = Column(String(45))
    visibility = Column(String(255))
    workflow = Column(String(255))
    journal_uuid = Column(String(255), ForeignKey(Journal.uuid))
    journal = relationship(
        Journal
    )
    authors = relationship(
        Author,
        secondary='author_has_publication'
    )
    electronicversions = relationship(
        ElectronicVersion,
        secondary='electronicversion_has_publication'
    )
    organizations = relationship(
        Organization,
        secondary='organization_has_publication'
    )
    woss = relationship(
        Wos,
        secondary='wos_has_publication'
    )
    deleted = Column(Boolean, default=False) # Records marked as deleted in Pure get True in this column.
    created = Column(
        Date,
        default=func.date(func.now())
    )
    updated = Column(
        Date,
        onupdate=func.date(func.now())
    )

# The following are linking tables for many-to-many relations.
class AuthorHasPublication(Base):
    __tablename__ = 'author_has_publication'
    author_uuid = Column(String(255), ForeignKey(Author.uuid), primary_key=True)
    publication_uuid = Column(String(255), ForeignKey(Publication.uuid), primary_key=True)

class ElectronicVersionHasPublication(Base):
    __tablename__ = 'electronicversion_has_publication'
    electronicversion_id = Column(String(255), ForeignKey(ElectronicVersion.id), primary_key=True)
    publication_uuid = Column(String(255), ForeignKey(Publication.uuid), primary_key=True)

class OrganizationHasPublication(Base):
    __tablename__ = 'organization_has_publication'
    organization_uuid = Column(String(255), ForeignKey(Organization.uuid), primary_key=True)
    publication_uuid = Column(String(255), ForeignKey(Publication.uuid), primary_key=True)


class WosHasPublication(Base):
    __tablename__ = 'wos_has_publication'
    wos_ut = Column(String(255), ForeignKey(Wos.ut), primary_key=True)
    publication_uuid = Column(String(255), ForeignKey(Publication.uuid), primary_key=True)

class ScopusAuthorHasScopus(Base):
    __tablename__ = 'scopusauthor_has_scopus'
    scopus_eid = Column(String(255), ForeignKey(Scopus.eid), primary_key=True)
    scopusauthor_auid = Column(String(255), ForeignKey(ScopusAuthor.auid), primary_key=True)
    seq = Column(Integer)

class ScopusHasPublication(Base):
    __tablename__ = 'scopus_has_publication'
    scopus_eid = Column(String(255), ForeignKey(Scopus.eid), primary_key=True)
    publication_uuid = Column(String(255), ForeignKey(Publication.uuid), primary_key=True)

class UnPaywallHasPublication(Base):
    __tablename__ = 'unpaywall_has_publication'
    doi = Column(String(255), ForeignKey(UnPaywall.doi), primary_key=True)
    publication_uuid = Column(String(255), ForeignKey(Publication.uuid), primary_key=True)

# will create tables if they don't exist
Base.metadata.create_all(engine)
