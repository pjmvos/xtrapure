import scopus.retrieve
import db.schema as db
from datetime import datetime

def get_scopus_changes(force_full=False,days_between_get_all=10000):
    '''
    if force_full==false get the latest date this job has run from the jobs table and run the script for all
    new and changed records in the publication table since that date.

    if true, run the script for all the records in the publication table

    :param force_full:
    :param days_between_get_all:
    :return:
    '''
    start = datetime.now()

    full_job_name = 'get scopus all'
    change_job_name = 'get scopus changes'

    s = db.Session()
    start_total = s.query(db.Scopus).count()

    last_full_job = s.query(db.Jobs).filter(db.Jobs.action == full_job_name).order_by(db.Jobs.job_date.desc()).limit(1).first()
    elapsed=datetime.now().date()-last_full_job.job_date.date()
    s.close()
    print('last ran get scopus all %s days ago' % (elapsed.days))
    if (elapsed.days >= (days_between_get_all)) or (force_full):
        print('start get scopus all')
        retrieve_from_date = datetime.strptime('20000101', "%Y%m%d").date() # equivalent to all
        job_name=full_job_name
    else:
        print('start get scopus changes')
        lastjob = s.query(db.Jobs).filter(db.Jobs.action == change_job_name).order_by(db.Jobs.job_date.desc()).limit(1).first()
        retrieve_from_date = lastjob.job_date.date()
        job_name=change_job_name
    article_data_list, article_data_list_scopusid = scopus.retrieve.pub_changes_byall(retrieve_from_date)

    db_changes = scopus.retrieve.retrieve_by_all(article_data_list, article_data_list_scopusid)
    s = db.Session()
    end_total = s.query(db.Scopus).count()
    end = datetime.now()
    job = db.Jobs(
        action=job_name,
        changes=len(article_data_list),
        db_changes=db_changes,
        start_total=start_total,
        end_total=end_total,
        duration=(end - start).seconds
    )
    s.add(job)
    s.commit()
    s.close()