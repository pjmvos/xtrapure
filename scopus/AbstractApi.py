import requests_cache, requests
import collections
import datetime

from conf.config import SCOPUS_APIKEY, SCOPUS_INSTTOKEN, SCOPUS_ABSTRACT_URL
from lxml import etree
from scopus.model import ScopusAuthor, ScopusCorrespondence, ScopusAuthhorAffiliation, ScopusPublication

# requests_cache.install_cache(cache_name='scopus_abstracts_requests_cache', allowable_methods=('GET', 'POST'))

NS_ABSTRACT = {
    'x': 'http://www.elsevier.com/xml/svapi/abstract/dtd',
    'dn': 'http://www.elsevier.com/xml/svapi/abstract/dtd',
    'ait': 'http://www.elsevier.com/xml/ani/ait',
    'ce': 'http://www.elsevier.com/xml/ani/common',
    'cto': 'http://www.elsevier.com/xml/cto/dtd',
    'dc': 'http://purl.org/dc/elements/1.1/',
    'prism': 'http://prismstandard.org/namespaces/basic/2.0/',
    'xocs': 'http://www.elsevier.com/xml/xocs/dtd',
    'xsi': 'http://www.w3.org/2001/XMLSchema-instance'
}


# https://api.elsevier.com/documentation/ScopusSearchAPI.wadl
# https://dev.elsevier.com/tips/ScopusSearchTips.htm

class ScopusData(ScopusPublication):
    def parse_xml(self, root, core_root):
        '''
        parse the data into our internal properties.

        For readability we use two roots:
        root = r
        coredata = r.xpath('./x:coredata', namespaces=NS_ABSTRACT)


        :param root:
        :param core_root:
        :return:
        '''
        # see: https://api.elsevier.com/documentation/guides/ScopusSearchViews.htm
        self.namespaces = NS_ABSTRACT
        self.root = root
        self.croot = core_root
        self.pub_date = self._pub_date()
        self.doi = self._doi()
        self.scopusid = self._scopusid()
        self.pmid = self._pmid()
        self.refartnum = self._refartnum()  # article number
        self.eissn = self._eissn()
        self.issn = self._issn()
        self.authors = self._authors()
        self.correspondence = self._correspondence()
        self.title = self._title()  # article title/book title(?)
        self.volume = self._volume()
        self.issue = self._issue()
        self.pages = self._pages()  # <first>-<last>
        self.citedby_count = self._citedby_count()

    def _doi(self):
        t = self.croot.xpath('./prism:doi/text()', namespaces=self.namespaces)
        return str(t[0]).strip() if len(t) > 0 else ''

    def _scopusid(self):
        t = self.croot.xpath('./x:eid/text()', namespaces=self.namespaces)
        return str(t[0]).strip() if len(t) > 0 else ''

    def _pmid(self):
        t = self.croot.xpath('./x:pubmed-id/text()', namespaces=self.namespaces)
        return str(t[0]).strip() if len(t) > 0 else ''

    def _refartnum(self):
        t = self.croot.xpath('./x:article-number/text()', namespaces=self.namespaces)
        return str(t[0]).strip() if len(t) > 0 else ''

    def _eissn(self):
        t = self.croot.xpath('./prism:eIssn/text()', namespaces=self.namespaces)
        return str(t[0]).strip() if len(t) > 0 else ''

    def _issn(self):
        t = self.croot.xpath('./prism:issn/text()', namespaces=self.namespaces)
        return str(t[0]).strip() if len(t) > 0 else ''

    def _pub_date(self):
        t = self.croot.xpath('./prism:coverDate/text()', namespaces=self.namespaces)
        if len(t) > 0:
            return datetime.datetime.strptime(str(t[0]), "%Y-%m-%d").date()
        else:
            return None

    def _title(self):
        t = self.croot.xpath('./dc:title/text()', namespaces=self.namespaces)
        return str(t[0]).strip() if len(t) > 0 else ''

    def _volume(self):
        t = self.croot.xpath('./prism:volume/text()', namespaces=self.namespaces)
        return str(t[0]).strip() if len(t) > 0 else ''

    def _issue(self):
        t = self.croot.xpath('./prism:issueIdentifier/text()', namespaces=self.namespaces)
        return str(t[0]).strip() if len(t) > 0 else ''

    def _pages(self):
        t = self.croot.xpath('./prism:pageRange/text()', namespaces=self.namespaces)
        return str(t[0]).strip() if len(t) > 0 else ''

    def _citedby_count(self):
        t = self.croot.xpath('./x:citedby-count/text()', namespaces=self.namespaces)
        return int(t[0]) if len(t) > 0 else 0

    def _authors(self):
        list = []
        for a in self.root.xpath('./x:authors/x:author', namespaces=self.namespaces):
            author = ScopusAuthor()
            author.auid = a.xpath('./@auid', namespaces=self.namespaces)[0]
            author.seq = int(a.xpath('./@seq', namespaces=self.namespaces)[0])
            author.given_name = a.findtext('./x:preferred-name/ce:given-name', namespaces=self.namespaces)
            author.initials = a.findtext('./ce:initials', namespaces=self.namespaces)
            author.surname = a.findtext('./ce:surname', namespaces=self.namespaces)
            author.indexed_name = a.findtext('./ce:indexed-name', namespaces=self.namespaces)
            af_list = []
            for af in a.xpath('./x:affiliation', namespaces=self.namespaces):
                afil = ScopusAuthhorAffiliation()
                afil.afid = af.xpath('./@id', namespaces=self.namespaces)[0]
                af_list.append(afil)
            author.affiliations = af_list
            list.append(author)
        return list

    def _correspondence(self):
        c = ScopusCorrespondence()
        # node = self.root.xpath('./x:item/x:bibrecord', namespaces=self.namespaces)
        # Why is the x namespace not needed???
        cnode = self.root.find('./item/bibrecord/head/correspondence', namespaces=self.namespaces)
        if cnode is not None:
            t = cnode.xpath('./person/ce:surname/text()', namespaces=self.namespaces)
            c.surname = str(t[0]) if len(t) > 0 else ''
            t = cnode.xpath('./person/ce:initials/text()', namespaces=self.namespaces)
            c.initials = str(t[0]) if len(t) > 0 else ''
            t = cnode.xpath('./person/ce:given-name/text()', namespaces=self.namespaces)
            c.given_name = str(t[0]) if len(t) > 0 else ''
            t = cnode.xpath('./person/ce:indexed-name/text()', namespaces=self.namespaces)
            c.indexed_name = str(t[0]) if len(t) > 0 else ''
            organizations = cnode.xpath('./affiliation/organization/text()', namespaces=self.namespaces)
            if len(organizations) > 0:
                c.organization_last = organizations[len(organizations) - 1]  # pick the last
            t = cnode.xpath('./affiliation/country/text()', namespaces=self.namespaces)
            c.country = str(t[0]) if len(t) > 0 else ''
            t = cnode.xpath('./ce:e-address/text()', namespaces=self.namespaces)
            c.email = str(t[0]) if len(t) > 0 else ''
        return c


class Api:
    def __init__(self):
        self.article_data_list = collections.OrderedDict()
        self.STORE = True  # if true store results in db

    @staticmethod
    def _abstract_request(scopus_id):
        """
        Do the request and return contents of successful

        :param scopus_id:
        :return: content, cached boolean
        """
        headers = {'Accept': 'application/xml'}
        params = {
            'apiKey': SCOPUS_APIKEY,
            'insttoken': SCOPUS_INSTTOKEN,
        }
        res = requests.get('%s/%s/%s' % (SCOPUS_ABSTRACT_URL, 'scopus_id', scopus_id), headers=headers, params=params)
        try:
            cached = res.from_cache
        except:
            cached = False
        if res.status_code == 200:
            return res.content, cached
        else:
            print(res.url)
            print(('*** Got: status_code: %s' % res.status_code))
            print('*** skipped')
            return False, cached

    @staticmethod
    def _parse_abstract_response(response_xml):
        """
        load the xml string into an etree object and parse it ito a ScopusData object

        :param response_xml
        :type string
        :return:
        """
        article_data = ScopusData()
        r = etree.fromstring(response_xml)
        coredata = r.xpath('./x:coredata', namespaces=NS_ABSTRACT)
        root = r
        if len(coredata) == 0:
            print('scopus_id not found')
        else:
            article_data.parse_xml(root=root, core_root=coredata[0])
        return len(coredata), article_data

    def _do_requests(self):
        """
        loop through the article_data_list and:
        request the data
        parse the data
        store the data in the database

        :return:
        """
        i = 0
        n = 10
        totalfound = 0
        scopuscnt = 0
        scopusfound = 0
        count = len(self.article_data_list)
        end = False
        start = datetime.datetime.now()
        step = 0
        total_time = 0
        for uuid in self.article_data_list:
            article = self.article_data_list[uuid]

            scopuscnt = scopuscnt + 1
            if end and start and ((i % n) == 0):
                elapsed = end - start
                if total_time == 0:
                    total_time = elapsed
                    average = elapsed
                else:
                    total_time = total_time + elapsed
                    average = ((step * average) + elapsed) / (step + 1)
                remaining = ((count - i) / n) * average
                step = step + 1
                start = datetime.datetime.now()
                print("\r" + str(i) + " | " + str(count - i) + " | elapsed: %s | average: %s | time remaining: %s" % (
                    total_time, average, remaining), end='')

            response, cached = self._abstract_request(article.scopusid)
            end = datetime.datetime.now()
            if response:
                total, result_data = self._parse_abstract_response(response)
                if total == 1:
                    scopusfound = scopusfound + 1
                    result_data.found_on = 'scopus_id'
                    totalfound = totalfound + 1
                    if self.STORE:
                        result_data.store(uuid)
                    else:
                        self.article_data_list[uuid] = result_data
            i = i + 1
        return totalfound

    def retrieve(self):
        '''
        start the process of retrieving the data

        :return:
        '''
        total = self._do_requests()
        return total
