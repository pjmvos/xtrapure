import db.schema as db

class ScopusAuthhorAffiliation:
    def __init__(self):
        self.afid=''
        self.dptid=''

class ScopusAuthor:
    def __init__(self):
        self.auid=''
        self.seq=0
        self.initials=''
        self.surname=''
        self.given_name=''
        self.indexed_name = ''
        self.affiliations=[]

class ScopusCorrespondence:
    # Note this can't be linked to ScopusAuthor, no auid is given!
    def __init__(self):
        self.initials=''
        self.surname=''
        self.indexed_name=''
        self.given_name=''
        self.country=''
        self.organization_last=''
        self.email=''

class ScopusPublication:
    def __init__(self):
        self.pub_date = None
        self.doi = ''
        self.scopusid = ''
        self.pmid = ''
        self.refartnum = ''  # article number
        self.eissn = ''
        self.issn = ''
        self.authors = []  # List of ScopusAuthor
        self.correspondence = None # ScopusCorrespondence only one?
        self.pubyear = ''
        self.title = ''  # article title/book title(?)
        self.volume = ''
        self.issue = ''
        self.pages = ''  # <first>-<last>
        self.citedby_count = 0
        self.found_on = ''


    def store(self, uuid):
        '''
        store the scopuspublication object in the database

        :param uuid:
        :return: -
        '''
        article_data = self
        s = db.Session()
        if s.query(db.Scopus).filter(db.Scopus.eid == article_data.scopusid).count() == 0:
            scopus = db.Scopus(
                eid=article_data.scopusid,
                doi=article_data.doi,
                pmid=article_data.pmid,
                refartnum=article_data.refartnum,
                eissn=article_data.eissn,
                issn=article_data.issn,
                pub_date=article_data.pub_date,
                title=article_data.title,
                volume=article_data.volume,
                issue=article_data.issue,
                pages=article_data.pages,
                citedby_count=article_data.citedby_count,
                found_on=article_data.found_on
            )
        else:
            scopus = s.query(db.Scopus).filter(db.Scopus.eid == article_data.scopusid).first()
            scopus.eid = article_data.scopusid
            scopus.doi = article_data.doi
            scopus.pmid = article_data.pmid
            scopus.refartnum = article_data.refartnum
            scopus.eissn = article_data.eissn
            scopus.issn = article_data.issn
            scopus.pub_date = article_data.pub_date
            scopus.title = article_data.title
            scopus.volume = article_data.volume
            scopus.issue = article_data.issue
            scopus.pages = article_data.pages
            scopus.citedby_count = article_data.citedby_count
            scopus.found_on = article_data.found_on
        s.add(scopus)
        pub = s.query(db.Publication).filter(db.Publication.uuid == uuid).first()
        if pub not in scopus.scopuss:
            scopus.scopuss.append(pub)
        s.flush()

        current_author_auid_list = []
        for aut in article_data.authors:
            current_author_auid_list.append(aut.auid)
            if s.query(db.ScopusAuthor).filter(db.ScopusAuthor.auid == aut.auid).count() == 0:
                author = db.ScopusAuthor(
                    auid=aut.auid,
                    initials=aut.initials,
                    surname=aut.surname,
                    given_name=aut.given_name,
                    indexed_name=aut.indexed_name
                )
            else:
                author = s.query(db.ScopusAuthor).filter(db.ScopusAuthor.auid == aut.auid).first()
                author.auid = aut.auid
                author.initials = aut.initials
                author.surname = aut.surname
                author.given_name = aut.given_name
                author.indexed_name = aut.indexed_name
            s.add(author)
            if scopus not in author.scopuss:
                author.scopuss.append(scopus)
            s.flush()

            shsa = s.query(db.ScopusAuthorHasScopus).filter(
                db.ScopusAuthorHasScopus.scopusauthor_auid == aut.auid, db.ScopusAuthorHasScopus.scopus_eid == article_data.scopusid).first()
            shsa.seq = aut.seq
            s.add(shsa)

            for af in aut.affiliations:
                q = s.query(db.ScopusAuthorHasAffiliation).filter(db.ScopusAuthorHasAffiliation.afid == af.afid, db.ScopusAuthorHasAffiliation.auid == aut.auid)
                if q.count()==0:
                    affiliation = db.ScopusAuthorHasAffiliation(
                         auid=aut.auid,
                         afid=af.afid
                        )
                    s.add(affiliation)
                    s.flush()
                q = s.query(db.ScopusAuthorAffiliations).filter(db.ScopusAuthorAffiliations.eid==article_data.scopusid,db.ScopusAuthorAffiliations.auid==aut.auid,db.ScopusAuthorAffiliations.afid==af.afid)
                if q.count()==0:
                    authoraffiliation = db.ScopusAuthorAffiliations(
                        eid = article_data.scopusid,
                        auid = aut.auid,
                        afid = af.afid
                    )
                    s.add(authoraffiliation)
                    s.flush
        s.flush()

        for author in scopus.authors:
            if author.auid not in current_author_auid_list:
                print('**** Remove author %s in scopus publication %s' % (author.auid, article_data.scopusid))
                q = s.query(db.ScopusAuthor).filter(db.ScopusAuthor.auid == author.auid)
                scopus.authors.remove(q.first())
        s.flush()

        if article_data.correspondence is not None:
            if s.query(db.ScopusCorrespondence).filter(
                    db.ScopusCorrespondence.eid == article_data.scopusid).count() == 0:
                correspondence = db.ScopusCorrespondence(
                    eid=article_data.scopusid,
                    initials=article_data.correspondence.initials,
                    surname=article_data.correspondence.surname,
                    indexed_name=article_data.correspondence.indexed_name,
                    given_name=article_data.correspondence.given_name,
                    country=article_data.correspondence.country,
                    organization_last=article_data.correspondence.organization_last,
                    email=article_data.correspondence.email
                )
            else:
                correspondence = s.query(db.ScopusCorrespondence).filter(
                    db.ScopusCorrespondence.eid == article_data.scopusid).first()
                correspondence.eid = article_data.scopusid,
                correspondence.initials = article_data.correspondence.initials,
                correspondence.surname = article_data.correspondence.surname,
                correspondence.indexed_name = article_data.correspondence.indexed_name,
                correspondence.given_name = article_data.correspondence.given_name,
                correspondence.country = article_data.correspondence.country,
                correspondence.organization_last = article_data.correspondence.organization_last,
                correspondence.email = article_data.correspondence.email
            s.add(correspondence)

        s.commit()
        s.close()