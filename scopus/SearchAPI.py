import requests_cache, requests
import collections
import datetime

import Levenshtein
from conf.config import SCOPUS_APIKEY, SCOPUS_INSTTOKEN, SCOPUS_SEARCH_URL
from lxml import etree
from scopus.model import ScopusPublication

#requests_cache.install_cache(cache_name='cache/scopus_requests_cache', allowable_methods=('GET', 'POST'),
#                             expire_after=3600 * 24 * 7)

NS = {
    'prism': 'http://prismstandard.org/namespaces/basic/2.0/',
    'opensearch': 'http://a9.com/-/spec/opensearch/1.1/',
    'dc': 'http://purl.org/dc/elements/1.1/',
    'x': 'http://www.w3.org/2005/Atom'
}


# https://api.elsevier.com/documentation/ScopusSearchAPI.wadl
# https://dev.elsevier.com/tips/ScopusSearchTips.htm

class ScopusData(ScopusPublication):
    '''
    Extend the ScopusPublicaton model with a method to parse the XML returned by the scopus search api
    '''
    @staticmethod
    def get_request_fields():
        return ['DOI', 'PMID', 'REFARTNUM', 'EISSN', 'ISSN', 'authors', 'PUBYEAR', 'TITLE', 'VOLUME', 'ISSUE',
                'PAGES']

    def parse_xml(self, root, core_root):
        # see: https://api.elsevier.com/documentation/guides/ScopusSearchViews.htm
        self.namespaces=NS
        self.root = root
        self.croot = core_root
        self.pub_date = self._pub_date()
        self.doi = self._doi()
        self.scopusid = self._scopusid()
        self.pmid = self._pmid()
        self.refartnum = self._refartnum()  # article number
        self.eissn = self._eissn()
        self.issn = self._issn()
        self.title = self._title()  # article title/book title(?)
        self.volume = self._volume()
        self.issue = self._issue()
        self.pages = self._pages()  # <first>-<last>
        self.citedby_count = self._citedby_count()


    def _doi(self):
        t = self.croot.xpath('./prism:doi/text()', namespaces=self.namespaces)
        return str(t[0]).strip() if len(t) > 0 else ''

    def _scopusid(self):
        t = self.croot.xpath('./x:eid/text()', namespaces=self.namespaces)
        return str(t[0]).strip() if len(t) > 0 else ''

    def _pmid(self):
        t = self.croot.xpath('./x:pubmed-id/text()', namespaces=self.namespaces)
        return str(t[0]).strip() if len(t) > 0 else ''

    def _refartnum(self):
        t = self.croot.xpath('./x:article-number/text()', namespaces=self.namespaces)
        return str(t[0]).strip() if len(t) > 0 else ''

    def _eissn(self):
        t = self.croot.xpath('./prism:eIssn/text()', namespaces=self.namespaces)
        return str(t[0]).strip() if len(t) > 0 else ''

    def _issn(self):
        t = self.croot.xpath('./prism:issn/text()', namespaces=self.namespaces)
        return str(t[0]).strip() if len(t) > 0 else ''

    def _pub_date(self):
        t = self.croot.xpath('./prism:coverDate/text()', namespaces=self.namespaces)
        if len(t) > 0:
            return datetime.datetime.strptime(str(t[0]), "%Y-%m-%d").date()
        else:
            return None

    def _title(self):
        t = self.croot.xpath('./dc:title/text()', namespaces=self.namespaces)
        return str(t[0]).strip() if len(t) > 0 else ''

    def _volume(self):
        t = self.croot.xpath('./prism:volume/text()', namespaces=self.namespaces)
        return str(t[0]).strip() if len(t) > 0 else ''

    def _issue(self):
        t = self.croot.xpath('./prism:issueIdentifier/text()', namespaces=self.namespaces)
        return str(t[0]).strip() if len(t) > 0 else ''

    def _pages(self):
        t = self.croot.xpath('./prism:pageRange/text()', namespaces=self.namespaces)
        return str(t[0]).strip() if len(t) > 0 else ''

    def _citedby_count(self):
        t = self.croot.xpath('./x:citedby-count/text()', namespaces=self.namespaces)
        return int(t[0]) if len(t) > 0 else 0


class Api:
    '''
    Contains the methods to retrieve data from the scopus search api and parses and stores that data
    '''
    def __init__(self):
        self.article_data_list = collections.OrderedDict()
        self.STORE = True  # if true store results in db

    @staticmethod
    def _search_request(query):
        headers = {'Accept': 'application/xml'}
        params = {
            'apiKey': SCOPUS_APIKEY,
            'insttoken': SCOPUS_INSTTOKEN,
            'query': query
        }
        res = requests.get(SCOPUS_SEARCH_URL, headers=headers, params=params)
        try:
            cached = res.from_cache
        except:
            cached = False
        if res.status_code == 200:
            return res.content, cached
        else:
            print(res.url)
            print(('*** Got: status_code: %s' % res.status_code))
            print('*** skipped')
            return False, cached
            # raise Exception('*** Got: status_code: %s' % res.status_code)

    @staticmethod
    def _create_query_string(article_data):
        '''
        The scopus query is created here.

        If known, query by DOI else. author, title, pages and pubyear

        :param article_data:
        :return:
        '''
        ql = []
        has_doi = False
        if article_data.doi == '':
            for field in ScopusData.get_request_fields():
                if not (getattr(article_data, field.lower()) == '' or getattr(article_data, field.lower()) == []):
                    if field == 'authors':
                        qa = []
                        for author in article_data.authors:
                            qa.append('%s(%s)' % (field, getattr(article_data, field)))
                        ql.append((' AND ').join(qa))
                    elif field == 'TITLE':
                        ql.append('TITLE("%s")' % article_data.title.lower().replace('"', ''))
                    elif field == 'PAGES':
                        ql.append('PAGES(%s)' % article_data.pages.split('-')[0])  # page range does not work
                    elif field == 'PUBYEAR':
                        ql.append('PUBYEAR = %s' % int(article_data.pubyear))
                    else:
                        ql.append('%s(%s)' % (field, getattr(article_data, field.lower())))
        else:
            has_doi = True
            ql.append('%s(%s)' % ('DOI', article_data.doi))
        return (' AND ').join(ql), has_doi

    @staticmethod
    def _parse_search_response(response_xml):
        article_data = ScopusData()
        r = etree.fromstring(response_xml)
        total = r.xpath('./opensearch:totalResults/text()', namespaces=NS)[0]
        if int(total) > 1:
            print('***** total: %s' % total)
        if int(total) == 1:
            root = r.xpath('./x:entry', namespaces=NS)
            core_root = r.xpath('./x:entry', namespaces=NS)
            article_data.parse_xml(root=root[0], core_root=core_root[0])
        return int(total), article_data

    def _same_title(self, org_title, res_title):
        '''
        Use Levenshtein to test similarity between the original and found title

        :param org_title:
        :param res_title:
        :return:
        '''
        title_similarity = Levenshtein.ratio(org_title, res_title)
        if title_similarity > 0.75:
            return True
        elif title_similarity > 0.2:
            a = res_title.lower().split(':')  # chiropractic patients in the netherlands: a descriptive study
            if (len(a) > 1) & (len(a[0]) > 12):  # word count
                return self._same_title(org_title, a[0])
        return False

    def _do_requests(self):
        '''
        Main procedure.

        Note if multipe results are found we won't store the data

        :return:
        '''
        i = 0
        totalfound = 0
        doicnt = 0
        metacnt = 0
        doifound = 0
        metafound = 0
        mutlipleondoi=0
        mutlipleonmetadata = 0
        titlemissondoi=0
        titlemissonmetadata = 0
        for uuid in self.article_data_list:
            article = self.article_data_list[uuid]
            query, has_doi = self._create_query_string(article)

            if (i % 100) == 0:
                print('%s' % (i))

            if has_doi:
                doicnt = doicnt + 1
            else:
                metacnt = metacnt + 1

            response, cached = self._search_request(query)
            if response:
                total, result_data = self._parse_search_response(response)
                #print(total)
                if total == 1:
                    if self._same_title(org_title=article.title.replace('"', '').lower(), res_title=result_data.title.lower()):
                        if has_doi:
                            doifound = doifound + 1
                            result_data.found_on = 'doi'
                        else:
                            metafound = metafound + 1
                            result_data.found_on = 'metadata'
                        totalfound = totalfound + 1
                        if self.STORE:
                            result_data.store(uuid)
                        else:
                            self.article_data_list[uuid] = result_data
                    else:
                        if has_doi:
                            titlemissondoi = titlemissondoi +1
                        else:
                            titlemissonmetadata = titlemissonmetadata + 1
                elif total>1:
                    if has_doi:
                        mutlipleondoi = mutlipleondoi + 1
                    else:
                        mutlipleonmetadata = mutlipleonmetadata + 1
            i = i + 1
        return totalfound, doicnt, doifound, metacnt, metafound, titlemissondoi, titlemissonmetadata, mutlipleondoi, mutlipleonmetadata

    def retrieve(self):
        total = self._do_requests()
        return total

