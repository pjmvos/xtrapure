import scopus.SearchAPI as SearchScopus
import scopus.AbstractApi as AbstractScopus
import db.schema as db
import tools.tools as tools


def retrieve_search(article_data_list):
    '''
    Start retrieving publications in the list via the Scopus Search API

    :param article_data_list:
    :return: totalfound: total number of records found in Scopus (scopus id not known)
    '''
    s = SearchScopus.Api()
    print('start reading the list')
    s.article_data_list = article_data_list
    print('read %s records' % len(s.article_data_list))
    total = len(s.article_data_list)
    print('start retrieving items')
    totalfound, doicnt, doifound, metacnt, metafound, titlemissondoi, titlemissonmetadata, mutlipleondoi, mutlipleonmetadata, scopuscnt, scopusfound, titlemissonscopus, multipleonscopus = s.retrieve()
    print('searched for %s records' % total)
    print('searched for %s records by scopus id' % scopuscnt)
    print('searched for %s records by doi' % doicnt)
    print('searched for %s records by metadata' % metacnt)
    print('found %s records' % totalfound)
    print('found %s records by scopus id' % scopusfound)
    print('found %s records by doi' % doifound)
    print('found %s records by metadata' % metafound)

    print('%s records found by scopus id with different title' % titlemissonscopus)
    print('%s records found by doi with different title' % titlemissondoi)
    print('%s records found by metadata with different title' % titlemissonmetadata)

    print('%s records with more than 1 hit on scopus_id in scopus' % multipleonscopus)
    print('%s records with more than 1 hit on doi in scopus' % mutlipleondoi)
    print('%s records with more than 1 hit on metadata in scopus' % mutlipleonmetadata)
    return totalfound


def retrieve_abstract(article_data_list):
    '''
    Start retrieving publications in the list via the Scopus Abstract API (scopus id known)

    :param article_data_list:
    :return: totalfound: total number of records found in Scopus
    '''
    print('start reading the "on scopusid" list')
    a = AbstractScopus.Api()
    a.article_data_list = article_data_list
    print('read %s records' % len(a.article_data_list))
    total = len(a.article_data_list)
    print('start retrieving items')
    scopusfound, scopuscnt = a.retrieve()
    print('searched for %s records by scopus id' % total)

    print('found %s records by scopus id' % scopusfound)
    return scopusfound


def retrieve_by_all(article_data_list, article_data_list_scopusid):
    '''
    This is started in the daily jobs

    :param article_data_list:
    :param article_data_list_scopusid:
    :return:
    '''
    s = SearchScopus.Api()
    print('start reading the "on metadata" list')
    s.article_data_list = article_data_list
    print('read %s records' % len(s.article_data_list))
    total = len(s.article_data_list)
    print('start retrieving items')
    totalfound, doicnt, doifound, metacnt, metafound, titlemissondoi, titlemissonmetadata, mutlipleondoi, mutlipleonmetadata = s.retrieve()
    print('searched for %s records' % total)
    print('searched for %s records by doi' % doicnt)
    print('searched for %s records by metadata' % metacnt)
    print('found %s records' % totalfound)

    print('found %s records by doi' % doifound)
    print('found %s records by metadata' % metafound)

    print('%s records found by doi with different title' % titlemissondoi)
    print('%s records found by metadata with different title' % titlemissonmetadata)

    print('%s records with more than 1 hit on doi in scopus' % mutlipleondoi)
    print('%s records with more than 1 hit on metadata in scopus' % mutlipleonmetadata)

    print('start reading the "on scopusid" list')
    a = AbstractScopus.Api()
    a.article_data_list = article_data_list_scopusid
    print('read %s records' % len(a.article_data_list))
    total = len(a.article_data_list)
    print('start retrieving items')
    scopusfound = a.retrieve()
    print('searched for %s records by scopus id' % total)

    print('found %s records by scopus id' % scopusfound)
    return totalfound + scopusfound


def bydoi():
    s = db.Session()
    print('start query')
    list = s.query(db.Publication).filter((db.Publication.doi != '') & (db.Publication.scopusid == '')).all()

    print('create list %s items' % len(list))
    article_data_list = {}
    for pub in list:
        article_data = SearchScopus.ScopusData()
        article_data.doi = tools.parse_doi(pub.doi)
        article_data_list[pub.uuid] = article_data
    return article_data_list


'''
def byscopusid():
    s = db.Session()
    print('start query all Pure publications with scopusid')
    list = s.query(db.Publication).filter(db.Publication.scopusid != '').all()

    print('create list %s items' % len(list))
    article_data_list = {}
    for pub in list:
        article_data = AbstractScopus.ScopusData()
        article_data.scopusid = pub.scopusid
        article_data_list[pub.uuid] = article_data
    return article_data_list
'''

'''
def bymetadata():
    s = db.Session()
    print('start query')
    list = s.query(db.Publication).filter(db.Publication.scopusid == '').all()

    print('create list %s items' % len(list))
    article_data_list = {}
    for pub in list:
        article_data = SearchScopus.ScopusData()
        article_data.title = pub.title
        article_data.volume = pub.volume
        article_data.issue = pub.issue
        article_data.issn = pub.journal.issn
        article_data.pages = pub.pages
        if pub.pub_date is not None:
            print(str(pub.pub_date.year))
            article_data.pubyear = str(pub.pub_date.year)
        # article_data.authors = []
        # for author in pub.authors:
        #    article_data.author.append('%s, %s' % (author.lastname, author.firstname.replace('.', '')))
        article_data_list[pub.uuid] = article_data
    return article_data_list
'''

def pub_changes_byall(retrieve_date):
    '''
    Query the Publication table for records new or update on or after retrieve_data.

    Returns 2 lists:
    One with scopus ids, for all publications with a scopusid
    One with publication metadata, if the scopusid is unknown in the publication table

    :param retrieve_date:
    :return:
    '''
    s = db.Session()
    print('start query, retrieving from %s' % retrieve_date)
    list = s.query(db.Publication).filter(db.Publication.deleted == False).filter((
        db.Publication.updated >= retrieve_date)|(db.Publication.created >= retrieve_date)).all()

    print('create list %s items' % len(list))
    article_data_list_scopusid = {}
    article_data_list = {}
    for pub in list:

        if pub.scopusid == '':
            article_data = SearchScopus.ScopusData()
            article_data.doi = tools.parse_doi(pub.doi)
            article_data.title = pub.title
            article_data.volume = pub.volume
            article_data.issue = pub.issue
            article_data.issn = pub.journal.issn
            article_data.pages = pub.pages
            if pub.pub_date is not None:
                article_data.pubyear = str(pub.pub_date.year)
            article_data_list[pub.uuid] = article_data
        else:
            article_data = AbstractScopus.ScopusData()
            article_data.scopusid = pub.scopusid
            article_data_list_scopusid[pub.uuid] = article_data
    return article_data_list, article_data_list_scopusid
