
## !!Script in development!!

import requests_cache, requests
import collections
import datetime
import db.schema as db
from conf.config import SCOPUS_APIKEY, SCOPUS_INSTTOKEN, SCOPUS_AUTHOR_SEARCH_URL
from lxml import etree

requests_cache.install_cache(cache_name='cache/scopus_requests_cache', allowable_methods=('GET', 'POST'),
                             expire_after=3600 * 24 * 7)

NS = {
    'prism': 'http://prismstandard.org/namespaces/basic/2.0/',
    'opensearch': 'http://a9.com/-/spec/opensearch/1.1/',
    'dc': 'http://purl.org/dc/elements/1.1/',
    'x': 'http://www.w3.org/2005/Atom',
    'atom': 'http://www.w3.org/2005/Atom'
}


class ScopusAuthorName:
    def __init__(self):
        self.surname = ''
        self.given_name = ''
        self.initials = ''
        self.preferred = False


class ScopusAuthorData:
    def __init__(self):
        self.eid = ''
        self.names = []

    def parse_xml(self, root, namespaces):
        # see: https://api.elsevier.com/documentation/guides/ScopusSearchViews.htm
        self.namespaces = namespaces
        self.root = root
        self.eid = self._eid()

    def _eid(self):
        t = self.root.xpath('./x:eid/text()', namespaces=self.namespaces)
        return str(t[0]).strip() if len(t) > 0 else ''


class Api:
    def __init__(self):
        self.article_data_list = collections.OrderedDict()
        self.STORE = True  # if true store results in db

    @staticmethod
    def _search_request(query,size,offset):
        headers = {'Accept': 'application/xml'}
        params = {
            'apiKey': SCOPUS_APIKEY,
            'insttoken': SCOPUS_INSTTOKEN,
            'query': query
        }
        res = requests.get(SCOPUS_AUTHOR_SEARCH_URL, headers=headers, params=params)
        try:
            cached = res.from_cache
        except:
            cached = False
        if res.status_code == 200:
            return res.content, cached
        else:
            print(res.url)
            print(('*** Got: status_code: %s' % res.status_code))
            print('*** skipped')
            return False, cached

    @staticmethod
    def _store_result(article_data, uuid):
        s = db.Session()

    def _parse_search_response(response_xml):
        author_data = ScopusAuthorData()

    def _do_requests(self):
        # 50 tegelijk
        size=50


    def retrieve(self):
        total = self._do_requests()
        return total