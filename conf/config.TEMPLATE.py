DB_URL = 'mysql+pymysql://<user>:<pass>@<server>/pure'

PURE_APIKEY = '<key>'
PURE_RESEARCHOUTPUTS_URL = 'https://research.vu.nl/ws/api/511/research-outputs'
PURE_PERSONS_URL = 'https://research.vu.nl/ws/api/511/persons'
PURE_CHANGES_URL = 'https://research.vu.nl/ws/api/511/changes'
PURE_USERNAME = '<user>'
PURE_PASSWORD = '<pass>'


WOS_LAMR_URL = 'http://ws.isiknowledge.com:80/cps/xrpc'
WOS_SRC = 'app.id=VUcites,env.id=VUcitesEnv,partner.email=<email>@vu.nl'  # identifier sent with WoS requests, not required
WOS_WAIT = 2  # Wait time between WoS requests
WOS_LAMR_USER = '<user>'
WOS_LAMR_PASSWORD = '<pass>'

SCOPUS_INSTTOKEN = '<token>'
SCOPUS_APIKEY = '<key>'
SCOPUS_SEARCH_URL = 'https://api.elsevier.com/content/search/scopus'
SCOPUS_ABSTRACT_URL = 'https://api.elsevier.com/content/abstract'
SCOPUS_AUTHOR_SEARCH_URL = 'https://api.elsevier.com/content/search/author'

UNPAYWALL_URL = 'https://api.unpaywall.org/v2'
UNPAYWALL_EMAIL = 'p.j.m.vos@vu.nl'