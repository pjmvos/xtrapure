from conf.config import WOS_LAMR_URL,WOS_SRC,WOS_WAIT,WOS_LAMR_PASSWORD,WOS_LAMR_USER
import db.schema as db
from lxml import etree as ET
import itertools
import requests
import time
from xml.dom import minidom
import requests_cache
import collections

requests_cache.install_cache(cache_name='cache/wos_requests_cache', allowable_methods=('GET', 'POST'),
                             expire_after=3600 * 24 * 7)

def chunks(data, size=50):
    it = iter(data)
    for i in range(0, len(data), size):
        yield {k: data[k] for k in itertools.islice(it, size)}


def prettify(elem):
    """Return a pretty-printed XML string for the Element.
    """
    rough_string = ET.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")


class WosData:
    def __init__(self):
        self.doi = ''
        self.ut = ''
        self.pmid = ''
        self.an = ''  # article number
        self.issn = ''
        self.isbn = ''
        self.author = []  # List of names
        self.year = ''
        self.atitle = ''  # article title/book title(?)
        self.stitle = ''  # journal title/book series title
        self.volume = ''
        self.issue = ''
        self.spage = ''  # start page

        self.timesCited = None
        self.sourceURL = None
        self.citingArticlesURL = None
        self.relatedRecordsURL = None
        self.date_updated = None

    @staticmethod
    def get_request_fields():
        return ['doi', 'ut', 'pmid', 'an', 'issn', 'isbn', 'author', 'year', 'atitle', 'stitle', 'volume', 'issue',
                'spage']


class Api:
    def __init__(self):
        self.articleDataList = collections.OrderedDict()
        self.resultData = collections.OrderedDict()

        self.STORE=True # if true store results in db
        self.VALIDATE=True # if true validate the articledata (minimum number of fields)

    @staticmethod
    def _request(reqXML):
        headers = {"Content-type": "text/xml"}
        res = requests.post(WOS_LAMR_URL, headers=headers, data=reqXML)
        print("Used Cache: {0}".format(res.from_cache))
        if res.status_code == 200:
            return res.content, res.from_cache
        else:
            raise Exception('*** Got: status_code: %s' % res.status_code)
        return res.content, res.from_cache

    @staticmethod
    def _parse_response(responseXML):
        """
        parse the response XML

        :param responseXML: API webservice xml response
        :return resultData: list of key/value dict
        """
        resultData = {}
        root = ET.fromstring(responseXML)
        for map in root[0][0]:
            id = map.attrib['name']
            resultData[id] = {}
            for val in map[0]:
                resultData[id][val.attrib['name']] = val.text
                # if val.attrib['name']=='ut': print('found: %s, ut: %s' % (id, val.text))
        return resultData

    def _create_request_xml(self, al, returnFields=['timesCited', 'ut', 'doi', 'pmid']):
        """
        Build the XML to send to the API

        :param al: article data dict of maximum 50 characters
        :param returnFields: default ['timesCited','ut','doi'], could add: 'sourceURL','citingArticlesURL','relatedRecordsURL'
        :return: XML string
        """
        request = ET.Element('request', {
            'src': WOS_SRC,
            'xmlns': 'http://www.isinet.com/xrpc41'
        })

        fn = ET.SubElement(request, 'fn', {'name': 'LinksAMR.retrieve'})
        list = ET.SubElement(fn, 'list')

        # WHO'S REQUESTING
        # could put username/password under this, but we use IP based access
        mu = ET.SubElement(list, 'map')
        #u = ET.SubElement(mu, 'val', {'name': 'username'}).text=WOS_LAMR_USER
        #p = ET.SubElement(mu, 'val', {'name': 'password'}).text=WOS_LAMR_PASSWORD

        # WHAT'S REQUESTED
        m = ET.SubElement(list, 'map')
        l = ET.SubElement(m, 'list', {'name': 'WOS'})

        for field in returnFields:
            ET.SubElement(l, 'val').text = field

        # LOOKUP DATA
        ml = ET.SubElement(list, 'map')
        for id in al:
            articledata = self.articleDataList[id]
            m = ET.SubElement(ml, 'map', {'name': id})
            for field in WosData.get_request_fields():
                if not (getattr(articledata, field) == '' or getattr(articledata, field) == []):
                    if field == 'author':
                        l = ET.SubElement(m, 'list', {'name': 'authors'})
                        for author in articledata.author:
                            ET.SubElement(l, 'val').text = author
                    else:
                        ET.SubElement(m, 'val', {'name': field}).text = getattr(articledata, field)
        return ET.tostring(request)

    def _do_requests(self, numArticles=50):
        """

        :param numArticles: maximum number of articles to send in one request (max 50)
        :return:
        """
        # for i in range(0, len(self.articleDataList), numArticles):
        # print('%s to %s' % (i, i + numArticles))
        # sublist = itertools.islice(self.articleDataList.items(), i, i + numArticles)  # slice into parts of max numArticles
        i = 0
        totalfound = 0
        for sublist in chunks(self.articleDataList, size=numArticles):
            print('%s to %s...' % (i, i + numArticles)),
            i = i + numArticles
            requestXML = self._create_request_xml(sublist)
            response, cached = self._request(requestXML)
            resultData = self._parse_response(response)

            numfound = self._merge_results(resultData, sublist)
            totalfound = totalfound + numfound
            if not cached: time.sleep(WOS_WAIT)
        return totalfound

    def _merge_results(self, resultData, sublist):
        numfound = 0
        for id in sublist:
            if 'ut' in resultData[id]:
                numfound = numfound + 1
                for field in resultData[id]:
                    setattr(self.articleDataList[id], field, resultData[id][field])
                if self.STORE==True:
                    self._store_result(self.articleDataList[id], id)
        return numfound

    def _validate_article_data(self, articleData):
        if self.VALIDATE==False:
            return True
        if articleData.doi != '' or articleData.pmid != '' or articleData.ut != '':
            # got a unique identifier, should work
            return True
        # elif not (articleData.volume == ''and articleData.issue == ''):  # always need volume AND issue
        elif articleData.volume != '':
            if articleData.spage != '' or articleData.an != '':  # need an article number or the start page to identify the article
                if articleData.stitle != '':  # need a journal title to identify the journal
                    return True
                elif len(articleData.author) > 0 and articleData.issn != '':  # or else the ISSN and author(s)
                    return True
        return False

    def retrieve_cites(self):
        totalfound = self._do_requests()
        return totalfound

    def read_list_of_doi(self, doilist):
        """
        Read a dict of dois {<id>: <doi>, <id2>:<doi2> ...} into the article data list

        :param doilist: dict
        :return:
        """
        for id in doilist:
            article_data = WosData()
            article_data.doi = doilist[id]
            self.articleDataList[id] = article_data

    def read_list_of_ut(self, utlist):
        """
        Read a dict of UT/ISI nrs {<id>: <ut>, <id2>:<ut2> ...} into the article data list

        :param utlist: dict
        :return:
        """
        for id in utlist:
            articleData = WosData()
            articleData.ut = utlist[id]
            self.articleDataList[id] = articleData

    def read_article_data_list(self, data):
        """
        Read an ordered dict of WoSrecords into self.articleDataList

        :param data:
        :return: True if data added, False if one of the articles is not validated
        """
        for id in data:
            if self._validate_article_data(data[id]):
                self.articleDataList[id] = data[id]
        return True, 0

    def read_article_data(self, id, data):
        """
        Read data of one article correctly formatted as an ArticleTemplate

        :param id:
        :param data:
        :return: True if data added, False if data not validated
        """
        if self._validate_article_data(data[id]):
            self.articleDataList[id] = data
            return True
        return False

    @staticmethod
    def _store_result(article_data, id):
        if article_data.ut != '':
            s = db.Session()
            if s.query(db.Wos).filter(db.Wos.ut == article_data.ut).count() == 0:
                wos = db.Wos(
                    ut=article_data.ut,
                    doi=article_data.doi,
                    pmid=article_data.pmid,
                    times_cited=article_data.timesCited,
                )
            else:
                wos = s.query(db.Wos).filter(db.Wos.ut == article_data.ut).first()
                wos.doi = article_data.doi,
                wos.pmid = article_data.pmid,
                wos.times_cited = article_data.timesCited,
            s.add(wos)
            pub = s.query(db.Publication).filter(db.Publication.uuid == id).first()
            if pub not in wos.woss:
                wos.woss.append(pub)
            s.commit()
            s.close()


