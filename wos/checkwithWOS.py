import wos.LamrAPI as LamrWOS
import db.schema as db

def check(articleDataList):
    s = db.Session()
    w = LamrWOS.Api()
    w.VALIDATE = False
    w.STORE = False
    print('start validating the list')
    w.read_article_data_list(articleDataList) # do not validate or use a simpler validation...
    print('validated %s records' % len(w.articleDataList))
    print('start retrieving items')
    total = w.retrieve_cites()
    print('found %s records' % total)

    for id in w.articleDataList:
        pub = s.query(db.Publication).filter(db.Publication.uuid == id).first()
        pure_doi=pub.doi.replace('http://dx.doi.org/', '').strip()
        #print(id, w.articleDataList[id].doi, pure_doi, pub.wos_accnum)
        wos = db.WosCheckDoi(
            uuid=id,
            pure_doi=pure_doi,
            wos_doi=w.articleDataList[id].doi,
            wos_ut=w.articleDataList[id].ut,
            pure_ut=pub.wos_accnum
        )
        s.add(wos)
        s.commit()

def doicheck():
    s = db.Session()
    print('start query')
    list = s.query(db.Publication).filter(db.Publication.doi != '').all()
    print('create list %s items' % len(list))
    articleDataList = {}
    for pub in list:
        articleData = LamrWOS.WosData()
        articleData.ut = pub.wos_accnum
        if articleData.ut=='':
            articleData.volume = pub.volume
            articleData.title = pub.title
            articleData.issn = pub.journal.issn
            pages = pub.pages
            articleData.spage = pages.split('-')[0]
            if not pub.pub_date is None:
                articleData.year = str(pub.pub_date.year)
        articleDataList[pub.uuid] = articleData
    return articleDataList

check(doicheck())# lookup everything with a doi on WOS_accnum or metadata.