import wos.LamrAPI as LamrWOS
import db.schema as db
import tools.tools as tools

def retrieve(articleDataList):
    w = LamrWOS.Api()
    print('start validating the list')
    w.read_article_data_list(articleDataList)
    print('validated %s records' % len(w.articleDataList))
    print('start retrieving items')
    total = w.retrieve_cites()
    print('found %s records' % total)

def bywos():
    s = db.Session()
    print('start query')
    list = s.query(db.Publication).filter(db.Publication.wos_accnum != '').all()

    print('create list %s items' % len(list))
    articleDataList = {}
    for pub in list:
        articleData = LamrWOS.WosData()
        articleData.ut = pub.wos_accnum
        articleDataList[pub.uuid] = articleData
    return articleDataList

def bydoi():
    s = db.Session()
    print('start query')
    list = s.query(db.Publication).filter(db.Publication.doi != '').all()

    print('create list %s items' % len(list))
    articleDataList = {}
    for pub in list:
        articleData = LamrWOS.WosData()
        articleData.doi = tools.parse_doi(pub.doi)
        articleDataList[pub.uuid] = articleData
    return articleDataList

def bymetadata():
    s = db.Session()
    print('start query')
    list = s.query(db.Publication).filter(
        (db.Publication.wos_accnum == '') & (db.Publication.pmid == '') & (db.Publication.doi == '')).all()

    print('create list %s items' % len(list))
    articleDataList = {}
    for pub in list:
        articleData = LamrWOS.WosData()
        articleData.title = pub.title
        articleData.volume = pub.volume
        articleData.issn = pub.journal.issn
        pages = pub.pages
        articleData.spage = pages.split('-')[0]
        if not pub.pub_date is None:
            articleData.year = str(pub.pub_date.year)
        articleData.authors = []
        for author in pub.authors:
            articleData.author.append('%s, %s' % (author.lastname, author.firstname.replace('.', '')))
        articleDataList[pub.uuid] = articleData
    return articleDataList

retrieve(bywos())
retrieve(bymetadata())
retrieve(bydoi())



