import datetime

class UnPaywall_Location:
    def __init__(self):
        self.pmh_id = ''
        self.evidence = ''
        self.host_type = ''
        self.is_best = None
        self.license = ''
        self.updated = None
        self.url = ''
        self.url_for_landing_page = ''
        self.url_for_pdf = ''
        self.version = ''


class Unpaywall_Data:
    def __init__(self):
        self.doi = ''
        self.best_oa_location = ''
        self.data_standard = None
        self.genre = ''
        self.is_oa = None
        self.journal_is_in_doaj = None
        self.journal_is_oa = None
        self.journal_issns = ''
        self.journal_name = ''
        self.published_date = ''
        self.publisher = ''
        self.title = ''
        self.updated_oaidoi = None
        self.year = None
        self.oa_locations = []

    def parse_json(self, json_data):
        self.doi = json_data.get('doi','')
        if self.doi!='':
            if json_data['best_oa_location'] is not None:
                self.best_oa_location = json_data['best_oa_location']['url']
            self.data_standard = json_data['data_standard']
            self.genre = json_data['genre']
            self.is_oa = json_data['is_oa']
            self.journal_is_in_doaj = json_data['journal_is_in_doaj']
            self.journal_is_oa = json_data['journal_is_oa']
            self.journal_issns = json_data['journal_issns']
            self.journal_name = json_data['journal_name']
            self.published_date = json_data['published_date']
            self.publisher = json_data['publisher']
            self.title = json_data['title']
            #2018-01-28T08:41:17.683847
            self.updated_oaidoi = datetime.datetime.strptime(json_data['updated'][:19], '%Y-%m-%dT%H:%M:%S')
            self.year = json_data['year']
            for l in json_data['oa_locations']:
                oa_location = UnPaywall_Location()
                oa_location.pmh_id = l['pmh_id']
                oa_location.evidence = l['evidence']
                oa_location.host_type = l['host_type']
                oa_location.is_best = l['is_best']
                oa_location.license = l['license']
                if l['updated'] is not None:
                    oa_location.updated = datetime.datetime.strptime(l['updated'][:19], '%Y-%m-%dT%H:%M:%S')
                oa_location.url = l['url']
                oa_location.url_for_landing_page = l['url_for_landing_page']
                oa_location.url_for_pdf = l['url_for_pdf']
                oa_location.version = l['version']
                self.oa_locations.append(oa_location)

