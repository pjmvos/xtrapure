from conf.config import UNPAYWALL_EMAIL, UNPAYWALL_URL
import requests_cache, requests
import datetime
import db.schema as db
import json

from unpaywall.model import Unpaywall_Data

#requests_cache.install_cache(cache_name='cache/oaidoi_requests_cache', allowable_methods=('GET', 'POST'), expire_after=3600 * 24 * 7)

class Api():
    def __init__(self):
        self.article_data_list = {}
        self.STORE = True  # if true store results in db

    def _request(self, doi):
        headers = {}
        params = {
            'email': UNPAYWALL_EMAIL,
        }
        res = requests.get('%s/%s' % (UNPAYWALL_URL, doi), headers=headers, params=params)
        # print("Used Cache: {0}".format(res.from_cache))
        try:
            cached = res.from_cache
        except:
            cached = False
        if res.status_code == 200:
            return res.content, cached
        else:
            print(res.url)
            print(('*** Got: status_code: %s' % res.status_code))
            print('*** skipped')
            return False, cached

    @staticmethod
    def _store_result(article_data, uuid):

        s = db.Session()

        if s.query(db.UnPaywall).filter(db.UnPaywall.doi == article_data.doi).count() == 0:
            unpaywall = db.UnPaywall(
                doi=article_data.doi,
                #best_oa_location=location_id,
                data_standard=article_data.data_standard,
                genre=article_data.genre,
                is_oa=article_data.is_oa,
                journal_is_in_doaj=article_data.journal_is_in_doaj,
                journal_is_oa=article_data.journal_is_oa,
                journal_issns=article_data.journal_issns,
                journal_name=article_data.journal_name,
                published_date=article_data.published_date,
                publisher=article_data.publisher,
                title=article_data.title,
                updated_oaidoi=article_data.updated_oaidoi,
                year=article_data.year
            )
        else:
            unpaywall = s.query(db.UnPaywall).filter(db.UnPaywall.doi == article_data.doi).first()
            unpaywall.doi = article_data.doi
            #unpaywall.best_oa_location = location_id,
            unpaywall.data_standard = article_data.data_standard
            unpaywall.genre = article_data.genre
            unpaywall.is_oa = article_data.is_oa
            unpaywall.journal_is_in_doaj = article_data.journal_is_in_doaj
            unpaywall.journal_is_oa = article_data.journal_is_oa
            unpaywall.journal_is_oa = article_data.journal_is_oa
            unpaywall.journal_issns = article_data.journal_issns
            unpaywall.journal_name = article_data.journal_name
            unpaywall.published_date = article_data.published_date
            unpaywall.publisher = article_data.publisher
            unpaywall.title = article_data.title
            unpaywall.updated_oaidoi = article_data.updated_oaidoi
            unpaywall.year = article_data.year
        s.add(unpaywall)
        s.flush()


        for l in article_data.oa_locations:
            if s.query(db.UnPaywall_Location).filter(db.UnPaywall_Location.url == l.url).count() == 0:
                location = db.UnPaywall_Location(
                    pmh_id=l.pmh_id,
                    doi=article_data.doi,
                    evidence=l.evidence,
                    host_type=l.host_type,
                    is_best=l.is_best,
                    license=l.license,
                    updated=l.updated,
                    url=l.url,
                    url_for_landing_page=l.url_for_landing_page,
                    url_for_pdf=l.url_for_pdf,
                    version=l.version
                )
            else:
                location = s.query(db.UnPaywall_Location).filter(db.UnPaywall_Location.url == l.url).first()
                location.pmh_id = l.pmh_id
                location.doi = article_data.doi
                location.evidence = l.evidence
                location.host_type = l.host_type
                location.is_best = l.is_best
                location.license = l.license
                location.updated = l.updated
                location.url = l.url
                location.url_for_landing_page = l.url_for_landing_page
                location.url_for_pdf = l.url_for_pdf
                location.version = l.version
            s.add(location)
            s.flush()

        location_id=None
        if (article_data.best_oa_location!=''):
            location_id=s.query(db.UnPaywall_Location).filter(db.UnPaywall_Location.url == article_data.best_oa_location).first().id

        unpaywall.best_oa_location = location_id
        s.add(unpaywall)

        pub = s.query(db.Publication).filter(db.Publication.uuid == uuid).first()
        if pub not in unpaywall.oaidois:
            unpaywall.oaidois.append(pub)

        s.commit()
        s.close()

    def _do_requests(self):
        i = 0
        total = 0
        for uuid in self.article_data_list:
            article = self.article_data_list[uuid]
            response, cached = self._request(article.doi)
            if (i % 100) == 0:
                print('%s' % (i))
            i = i + 1
            if response:
                result_data = Unpaywall_Data()
                result_data.parse_json(json.loads(response.decode()))
                if result_data.doi!='':
                    if self.STORE:
                        self._store_result(result_data, uuid)
                    else:
                        self.article_data_list[uuid] = result_data
                    total = total + 1
        return total


    def retrieve(self):
        total = self._do_requests()
        return total
