import unpaywall.API as UP
import db.schema as db
import tools.tools as tools


def retrieve(article_data_list):
    o = UP.Api()
    print('start reading the list')
    o.article_data_list = article_data_list
    print('read %s records' % len(o.article_data_list))
    total = len(o.article_data_list)
    print('start retrieving items')
    totalfound = o.retrieve()
    print('searched for %s records' % total)
    print('found %s records' % totalfound)


def byall():
    s = db.Session()
    print('start query')
    list = s.query(db.Publication).filter(db.Publication.deleted == False).filter(db.Publication.doi != '').all()

    print('create list %s items' % len(list))
    article_data_list = {}
    for pub in list:
        article_data = UP.Unpaywall_Data()
        article_data.doi = tools.parse_doi(pub.doi)
        article_data_list[pub.uuid] = article_data
    return article_data_list


def pub_changes_byall(retrieve_date, start_index=0):
    s = db.Session()
    print('start query, retrieving from %s' % retrieve_date)
    list = s.query(db.Publication).filter(db.Publication.deleted == False).filter(
        (db.Publication.updated >= retrieve_date) | (db.Publication.created >= retrieve_date)).all()

    print('create list %s items' % len(list))
    article_data_list = {}
    n = 1
    for pub in list:
        article_data = UP.Unpaywall_Data()
        article_data.doi = tools.parse_doi(pub.doi)
        if n > start_index:
            article_data_list[pub.uuid] = article_data
        n = n + 1
    return article_data_list
