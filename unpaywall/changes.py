import unpaywall.retrieve as retrieveUnPaywall
import db.schema as db
from datetime import datetime

def get_unpaywall_changes(force_full=False, days_between_get_all=14, start_index=0):
    start = datetime.now()
    full_job_name = 'get unpaywall all'
    change_job_name='get unpaywall changes'

    s = db.Session()

    last_full_job = s.query(db.Jobs).filter(db.Jobs.action == full_job_name).order_by(db.Jobs.job_date.desc()).limit(1).first()
    elapsed_full = datetime.now().date() - last_full_job.job_date.date()

    print('last ran get unpaywall all %s days ago' % (elapsed_full.days))
    if (elapsed_full.days >= days_between_get_all) or (force_full):
        print('start get unpaywall all')
        article_data_list = retrieveUnPaywall.byall()
        job_name=full_job_name
    else:
        print('start get unpaywall changes')
        last_job = s.query(db.Jobs).filter(db.Jobs.action == change_job_name).order_by(db.Jobs.job_date.desc()).limit(
            1).first()
        retrieve_from_date = last_job.job_date.date()
        article_data_list = retrieveUnPaywall.pub_changes_byall(retrieve_date=retrieve_from_date,start_index=start_index)
        job_name = change_job_name

    start_total = s.query(db.UnPaywall).filter(db.UnPaywall.is_oa).count()
    s.close()

    db_changes = retrieveUnPaywall.retrieve(article_data_list)

    s = db.Session()
    end_total = s.query(db.UnPaywall).filter(db.UnPaywall.is_oa).count()
    end = datetime.now()
    job = db.Jobs(
        action=job_name,
        changes=len(article_data_list),
        db_changes=db_changes,
        start_total=start_total,
        end_total=end_total,
        duration=(end - start).seconds
    )
    s.add(job)
    s.commit()
    s.close()
