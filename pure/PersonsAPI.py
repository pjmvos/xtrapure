from conf.config import PURE_APIKEY, PURE_PERSONS_URL, PURE_USERNAME, PURE_PASSWORD
from lxml import etree as ET
from datetime import datetime
import pure.model as model
import requests
import requests_cache


requests_cache.install_cache(cache_name='pure_person_requests_cache', allowable_methods=('GET', 'POST'))

class Person(model.PureAuthor):
    '''
    Persons class based on the model Author class for parsing the xml

    '''

    def parse(self, root):
        self.root = root
        self.uuid = root.xpath('./@uuid')[0]
        self.firstname = self._firstname()
        self.lastname = self._lastname()
        self.digitalid, self.scopusid, self.researcherid, self.isni = self._id()
        self.orcid = self._orcid()

    def _firstname(self):
        t = self.root.find('./name/firstName')
        return t.text.strip() if t is not None else ''

    def _lastname(self):
        t = self.root.find('./name/lastName')
        return t.text.strip() if t is not None else ''

    def _orcid(self):
        t = self.root.find('./orcid')
        return t.text.strip() if t is not None else ''

    def _id(self):
        scopusid = ''
        digitalid = ''
        researcherid = ''
        isni = ''
        ids = self.root.xpath('./ids/id')
        for id in ids:
            type = id.find('./type').attrib['uri']
            value = id.find('./value').text.strip()
            if type == '/dk/atira/pure/person/personsources/scopusauthor':
                if scopusid!='':
                    print('multiple scopus id %s' % (self.uuid))
                    scopusid = '%s|%s' % (value, scopusid)
                else:
                    scopusid = value
            elif type == '/dk/atira/pure/person/personsources/digitalauthor':
                digitalid = value
            elif type == '/dk/atira/pure/person/personsources/researcher':
                researcherid = value
            elif type == '/dk/atira/pure/person/personsources/isni':
                isni = value
            else:
                print(type, self.uuid)

        return digitalid, scopusid, researcherid, isni


def _do_postrequest(data, size=10, offset=0):
    headers = {"Content-Type": "application/xml", 'Accept': 'application/xml'}
    payload = {'apiKey': PURE_APIKEY, 'size': size, 'offset': offset}

    res = requests.post(PURE_PERSONS_URL, headers=headers, params=payload, data=data,
                        auth=(PURE_USERNAME, PURE_PASSWORD))
    try:
        cached = res.from_cache
    except:
        cached = False
    if res.status_code == 200:
        return res, cached
    else:
        raise Exception('*** Got: status_code: %s' % res.status_code)


def _process_request(reqXML, size, offset):
    num_changes = 0
    res, cached = _do_postrequest(reqXML, size, offset)
    xml = res.content.decode('latin-1')
    parser = ET.XMLParser(recover=True, encoding='utf-8')
    r = ET.fromstring(xml, parser=parser)
    results = r.xpath('./items/person')
    for content in results:
        aut = Person()
        aut.parse(content)
        changed = aut.store()
        if changed:
            num_changes = num_changes + 1
    return cached, num_changes


def get_all():
    reqXML = '<?xml version="1.0"?><personsQuery>' \
             '<searchString>*</searchString>' \
             '</personsQuery>'

    res, cached = _do_postrequest(data=reqXML, size=1, offset=0)

    r = ET.fromstring(res.content.decode('latin-1'))
    count = int(r.xpath('./count/text()')[0])
    print('found %s records' % count)

    size = 10
    end = False
    start = False
    total_time = 0
    remaining = 0
    average = 0
    step = 0
    elapsed = 0
    # count=100
    for offset in range(0, count, size):
        if end and start:
            elapsed = end - start
            if total_time == 0:
                total_time = elapsed
                average = elapsed
            else:
                total_time = total_time + elapsed
                average = ((step * average) + elapsed) / (step + 1)
            remaining = ((count - offset) / size) * average
            step = step + 1
        print("\r" + str(offset) + " | " + str(count - offset) + " | Used Cache: {0}".format(cached) +
              " | time remaining: %s | average: %s | elapsed: %s" % (remaining, average, elapsed), end='')
        start = datetime.now()
        _process_request(reqXML=reqXML, size=size, offset=offset)
        # res, cached = _do_postrequest(data=reqXML, size=size, offset=offset)
        # _process(res)
        end = datetime.now()


def get_by_uuid_list(uuid_list=[]):
    """
    Gets the records by uuid from the Pure Research API. The returned XML records are parsed and changes and new records
    are stored in the local Pure database.
    This function can be used to retrieve the UUIDS returned by the Pure Changes API

    :param uuid_list:   list of uuid's of Pure Research Outputs
    :return: total_changes:    The number of changed database records
    """
    n = 10
    end = False
    start = False
    total_time = 0
    remaining = 0
    average = 0
    step = 0
    count = len(uuid_list)
    cached = False
    total_changes = 0
    for i in range(0, count, n):
        uuids = ('</uuid><uuid>').join(uuid_list[i:i + n])
        reqXML = '<?xml version="1.0"?>' \
                 '<personsQuery>' \
                 '<uuids><uuid>%s</uuid></uuids>' \
                 '</personsQuery>' % uuids
        if end and start:
            elapsed = end - start
            if total_time == 0:
                total_time = elapsed
                average = elapsed
            else:
                total_time = total_time + elapsed
                average = ((step * average) + elapsed) / (step + 1)
            remaining = ((count - i) / n) * average
            step = step + 1
            print("\r" + str(i) + " | " + str(count - i) + " | Used Cache: {0}".format(cached) +
                  " | time remaining: %s | changed in database: %s" % (remaining, total_changes), end='')

        start = datetime.now()
        cached, num_changes = _process_request(reqXML=reqXML, size=n, offset=0)
        total_changes = total_changes + num_changes
        end = datetime.now()
    return total_changes


def set_deletes(uuid_deleted_list=[]):
    p = model.PureAuthor()
    for uuid in uuid_deleted_list:
        p.setDeleted(uuid)
    print('%s authors set to deleted' % len(uuid_deleted_list))
    return len(uuid_deleted_list)
