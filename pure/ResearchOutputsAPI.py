from conf.config import PURE_APIKEY, PURE_RESEARCHOUTPUTS_URL, PURE_USERNAME, PURE_PASSWORD
from lxml import etree as ET
from datetime import datetime
import pure.model as model
import re
import requests
import requests_cache
import itertools
import time

# requests_cache.install_cache(cache_name='cache/pure_requests_cache', allowable_methods=('GET', 'POST'))

NS = {
    'xsi': 'http://www.w3.org/2001/XMLSchema-instance'
}


def chunks(data, size=50):
    it = iter(data)
    for i in range(0, len(data), size):
        yield {k: data[k] for k in itertools.islice(it, size)}


class ContributionToJournal(model.PurePublication):
    def parse(self, root):
        """
        parse contributionToJournal from Pure ResearchOutputs API XML. The result is a PurePublication object which is
        stored in the database

        :param root: root node is r.xpath('./items/contributionToJournal')
        :return:
        """
        self.root = root
        self.uuid = root.xpath('./@uuid')[0]
        self.title = self._title()
        self.subtitle = self._subtitle()
        self.volume = self._volume()
        self.pages = self._pages()
        self.pubtype = self._pubtype()
        self.category = self._category()
        self.peer_review = self._peer_review()
        self.oa_permission = self._oa_permission()
        self.article_number = self._an()
        self.doi = self._doi()
        self.pub_date, self.epub_date = self._pub_date()
        jrn = model.PureJournal()
        jrn.uuid = self._journal_uuid()
        jrn.title = self._journal_title()
        jrn.issn = self._journal_issn()
        self.journal = jrn
        self.electronic_versions = self._electronic_versions()
        self.authors = self._authors()
        self.organisations = self._organisations()
        self.confidential = self._confidential()
        self.visibility = self._visibility()
        self.workflow = self._workflow()
        for source in self._sources():
            for name in source:
                setattr(self, name, source[name])
        changed = self.store()
        return changed

    def _title(self):
        return re.sub('\s+', ' ', self.root.find('./title').text.strip())

    def _subtitle(self):
        # 511 ?
        t = self.root.find('./subTitle')
        return re.sub('\s+', ' ', t.text.strip()) if t is not None else ''

    def _volume(self):
        t = self.root.find('./volume')
        return t.text.strip() if t is not None else ''

    def _pages(self):
        t = self.root.find('./pages')
        return t.text.strip() if t is not None else ''

    def _pubtype(self):
        # TODO check 518 XML
        t = self.root.find('./types/type')
        return t.attrib['uri'] if t is not None else ''

    def _category(self):
        # TODO check 518 XML
        t = self.root.find('./categories/category')
        return t.attrib['uri'] if t is not None else ''

    def _peer_review(self):
        # TODO check 518 XML
        t = self.root.find('./peerReview')
        return t.text.strip() if t is not None else ''

    def _oa_permission(self):
        # TODO check 518 XML
        t = self.root.find('./openAccessPermissions/openAccessPermission')
        return t.attrib['uri'] if t is not None else ''

    def _an(self):
        # 511 ?
        t = self.root.find('./articleNumber')
        return t.text.strip() if t is not None else ''

    def _doi(self):
        dois = []
        doi = ''
        for v in self.root.xpath('./electronicVersions/electronicVersion'):
            d = v.find('./doi')
            if d is not None:
                dois.append(d.text.strip())
            doi = ' | '.join(dois)
        return doi

    def _pub_date(self):
        pub_date = None
        epub_date = None
        for status in self.root.xpath('./publicationStatuses/publicationStatus'):
            y = status.find('./publicationDate/year').text
            t = status.find('./publicationDate/month')
            m = t.text if t is not None else 1
            t = status.find('./publicationDate/day')
            d = t.text if t is not None else 1

            # TODO check 518 XML
            t = status.find('./publicationStatuses/publicationStatus')
            if t is not None:
                # if status.find('./publicationStatus').attrib['uri'] == '/dk/atira/pure/publication/status/published': #changed in 511
                if t.attrib['uri'] == '/dk/atira/pure/researchoutput/status/published':
                    pub_date = datetime(int(y), int(m), int(d)).date()
                # if status.find('./publicationStatus').attrib['uri'] == '/dk/atira/pure/publication/status/epub': #changed in 511
                if t.attrib['uri'] == '/dk/atira/pure/researchoutput/status/epub':
                    epub_date = datetime(int(y), int(m), int(d)).date()
        return pub_date, epub_date

    def _journal_uuid(self):
        # TODO check 518 XML
        t = self.root.find('./journalAssociation/journal')
        return t.attrib['uuid'] if t is not None else ''

    def _journal_title(self):
        # TODO check 518 XML
        t = self.root.find('./journalAssociation/title')
        return t.text if t is not None else ''

    def _journal_issn(self):
        t = self.root.find('./journalAssociation/issn')
        return t.text.strip() if t is not None else ''

    def _electronic_versions(self):
        evs = self.root.xpath('./electronicVersions/electronicVersion')

        electronic_versions = []
        # TODO check 518 XML
        if evs is not None:
            for electronic_version in evs:
                ev = model.PureElectronicVersion()
                # ev.id = electronic_version.xpath('./@id')[0]
                ev.id = electronic_version.xpath('./@pureId')[0]
                ev.type = electronic_version.xpath('./@xsi:type', namespaces=NS)[0]
                t = electronic_version.find('./accessTypes/accessType')
                ev.access_type = t.attrib['uri'] if t is not None else ''

                t = electronic_version.find('./versionTypes/versionType')
                ev.version_type = t.attrib['uri'] if t is not None else ''

                t = electronic_version.find('./visibleOnPortalDate')
                ev.visible_on_portal_date = t.text.strip() if t is not None else ''

                t = electronic_version.find('./creator')
                ev.file_creator = t.text.strip() if t is not None else ''

                t = electronic_version.find('./created')
                ev.file_created = t.text.strip() if t is not None else ''

                t = electronic_version.find('./title')
                if t is not None and t.text is not None:
                    ev.file_title = t.text.strip()
                else:
                    ev.file_title = ''
                files = electronic_version.xpath('./file')
                evurls = []
                for file in files:
                    t = file.find('./fileURL')
                    if t is not None:
                        evurls.append(t.text.strip())
                ev.file_urls = '|'.join(evurls)
                ev.file_total = len(evurls)

                t = electronic_version.find('./doi')
                ev.doi = t.text.strip() if t is not None else ''

                t = electronic_version.find('./link')
                ev.link = t.text.strip() if t is not None else ''

                electronic_versions.append(ev)
        return electronic_versions

    def _authors(self):
        persons = self.root.xpath('./personAssociations/personAssociation')
        authors = []
        for person in persons:
            p = person.xpath('./person/@uuid')
            if len(p) > 0:  # anders istie external
                aut = model.PureAuthor()
                aut.uuid = p[0]
                t = person.xpath('./name/firstName/text()')
                aut.firstname = str(t[0]) if len(t) > 0 else ''
                # aut.firstname = person.find('./name/firstName').text
                aut.lastname = person.find('./name/lastName').text
                authors.append(aut)
        return authors

    def _organisations(self):
        o = self.root.xpath('./organisationalUnits/organisationalUnit')
        organisations = []
        for organisation in o:
            org = model.PureOrganization()
            org.uuid = organisation.xpath('./@uuid')[0]
            # TODO check 518 XML
            t=organisation.find('./names/name')
            org.name = t.text if t is not None else ''
            org.name = re.sub('\s+', ' ', org.name)
            organisations.append(org)
        return organisations

    def _sources(self):
        ids = []

        xid = self.root.xpath('./@externalId')
        if len(xid) > 0:
            id = xid[0]
            source = self.root.xpath('./@externalIdSource')[0]
            if source == 'WOS':
                ids.append({'wos_accnum': id})
            elif source == 'Scopus':
                ids.append({'scopusid': id})
            elif source == 'PubMed':
                ids.append({'pmid': id})
        for i in self.root.xpath('./info/additionalExternalIds/id'):
            id = i.text
            source = i.xpath('./@idSource')[0]
            if source == 'WOS':
                ids.append({'wos_accnum': id})
            elif source == 'Scopus':
                ids.append({'scopusid': id})
            elif source == 'PubMed':
                ids.append({'pmid': id})
        return ids

    def _sources59(self):
        # ExternalableInfo was removed in 511
        sources = self.root.xpath('./externalableInfo/secondarySources/secondarySource')
        ids = []
        for s in sources:
            source = s.find('./source').text
            id = s.find('./sourceId').text
            if source == 'WOS':
                ids.append({'wos_accnum': id})
            elif source == 'Scopus':
                ids.append({'scopusid': id})
            elif source == 'PubMed':
                ids.append({'pmid': id})

        sources = self.root.xpath('./externalableInfo')
        for s in sources:
            source = s.find('./source')
            if source is not None:
                id = s.find('./sourceId').text
                if source == 'WOS':
                    ids.append({'wos_accnum': id})
                elif source == 'Scopus':
                    ids.append({'scopusid': id})
                elif source == 'PubMed':
                    ids.append({'pmid': id})
        return ids

    def _confidential(self):
        t = self.root.find('./confidential')
        return t.text.strip() if t is not None else ''

    def _visibility(self):
        t = self.root.find('./visibilities/visibility')
        return t.text.strip() if t is not None else ''

    def _workflow(self):
        t = self.root.find('./workflows/workflow')
        return t.text.strip() if t is not None else ''


def _do_postrequest(data, size, offset, repeat=0):
    headers = {"Content-Type": "application/xml", 'Accept': 'application/xml'}
    payload = {'apiKey': PURE_APIKEY, 'size': size, 'offset': offset}

    try:
        res = requests.post(PURE_RESEARCHOUTPUTS_URL, headers=headers, params=payload, data=data,
                            auth=(PURE_USERNAME, PURE_PASSWORD))
        cached = False
        if res.status_code == 200:
            return res, cached
        else:
            if repeat <= 10:
                time.sleep(60 * (repeat + 1))
                _do_postrequest(data=data, size=size, offset=offset, repeat=repeat + 1)
            else:
                return False, False
    except:
        if repeat <= 10:
            print('An error occurred, retrying')
            time.sleep(60 * (repeat + 1))
            _do_postrequest(data=data, size=size, offset=offset, repeat=repeat + 1)
        else:
            return False, False


def _process(res):
    num_changes = 0
    # xml = res.content.decode('latin-1')
    xml = res.text
    parser = ET.XMLParser(recover=True, encoding='utf-8')
    r = ET.fromstring(xml, parser=parser)
    results = r.xpath('./items/contributionToJournal')
    for content in results:
        pub = ContributionToJournal()
        changed = pub.parse(content)
        if changed:
            num_changes = num_changes + 1
    return num_changes


def _process_request(reqXML, size, offset):
    num_changes = 0
    res, cached = _do_postrequest(reqXML, size, offset)
    # xml = res.content.decode('latin-1')
    xml = res.text

    parser = ET.XMLParser(recover=True, encoding='utf-8')
    r = ET.fromstring(xml, parser=parser)
    results = r.xpath('./items/contributionToJournal')
    for content in results:
        pub = ContributionToJournal()
        changed = pub.parse(content)
        if changed:
            num_changes = num_changes + 1
    return cached, num_changes


def get_all_contributiontojournal():
    reqXML = '<?xml version="1.0"?><researchOutputsQuery>' \
             '<typeUris>' \
             '<typeUri>/dk/atira/pure/researchoutput/researchoutputtypes/contributiontojournal/article</typeUri>' \
             '<typeUri>/dk/atira/pure/researchoutput/researchoutputtypes/contributiontojournal/letter</typeUri>' \
             '<typeUri>/dk/atira/pure/researchoutput/researchoutputtypes/contributiontojournal/comment</typeUri>' \
             '<typeUri>/dk/atira/pure/researchoutput/researchoutputtypes/contributiontojournal/book</typeUri>' \
             '<typeUri>/dk/atira/pure/researchoutput/researchoutputtypes/contributiontojournal/editorial</typeUri>' \
             '<typeUri>/dk/atira/pure/researchoutput/researchoutputtypes/contributiontojournal/special</typeUri>' \
             '<typeUri>/dk/atira/pure/researchoutput/researchoutputtypes/contributiontojournal/abstract</typeUri>' \
             '<typeUri>/dk/atira/pure/researchoutput/researchoutputtypes/contributiontojournal/systematicreview</typeUri>' \
             '<typeUri>/dk/atira/pure/researchoutput/researchoutputtypes/contributiontojournal/shortsurvey</typeUri>' \
             '<typeUri>/dk/atira/pure/researchoutput/researchoutputtypes/contributiontojournal/erratum</typeUri>' \
             '</typeUris>' \
             '</researchOutputsQuery>'

    res, cached = _do_postrequest(data=reqXML, size=1, offset=0)
    r = ET.fromstring(res.content.decode('latin-1'))
    count = int(r.xpath('./count/text()')[0])
    print('found %s records' % count)

    size = 10
    end = False
    start = False
    total_time = 0
    remaining = 0
    average = 0
    step = 0
    elapsed = 0
    total_changes = 0
    for offset in range(0, count, size):
        if end and start:
            elapsed = end - start
            if total_time == 0:
                total_time = elapsed
                average = elapsed
            else:
                total_time = total_time + elapsed
                average = ((step * average) + elapsed) / (step + 1)
            remaining = ((count - offset) / size) * average
            step = step + 1
        print("\r" + str(offset) + " | " + str(count - offset) + " | Used Cache: {0}".format(cached) +
              " | time remaining: %s | average: %s | elapsed: %s | changed: %s" % (
              remaining, average, elapsed, total_changes), end='')
        start = datetime.now()
        res, cached = _do_postrequest(data=reqXML, size=size, offset=offset)
        num_changes = _process(res)
        total_changes = total_changes + num_changes
        end = datetime.now()


def get_by_uuid_list(uuid_list=[]):
    """
    Gets the records by uuid from the Pure Research API. The returned XML records are parsed and changes and new records
    are stored in the local Pure database.
    This function can be used to retrieved the records returned by the Pure Changes API

    :param uuid_list:   list of uuid's of Pure Research Outputs
    :return: total_changes:    The number of changed database records
    """
    n = 10
    end = False
    start = False
    total_time = 0
    remaining = 0
    average = 0
    step = 0
    count = len(uuid_list)
    cached = False
    total_changes = 0
    for i in range(0, count, n):
        uuids = ('</uuid><uuid>').join(uuid_list[i:i + n])
        reqXML = '<?xml version="1.0"?>' \
                 '<researchOutputsQuery><uuids>' \
                 '<uuid>%s</uuid>' \
                 '</uuids></researchOutputsQuery>' % uuids
        if end and start:
            elapsed = end - start
            if total_time == 0:
                total_time = elapsed
                average = elapsed
            else:
                total_time = total_time + elapsed
                average = ((step * average) + elapsed) / (step + 1)
            remaining = ((count - i) / n) * average
            step = step + 1
            print("\r" + str(i) + " | " + str(count - i) + " | Used Cache: {0}".format(cached) +
                  " | time remaining: %s | changed in database: %s" % (remaining, total_changes), end='')

        start = datetime.now()
        cached, num_changes = _process_request(reqXML=reqXML, size=n, offset=0)
        total_changes = total_changes + num_changes
        end = datetime.now()
    return total_changes


def set_deletes(uuid_deleted_list=[]):
    p = model.PurePublication()
    for uuid in uuid_deleted_list:
        p.setDeleted(uuid)
    print('%s publications set to deleted' % len(uuid_deleted_list))
    return len(uuid_deleted_list)
