import db.schema as db

# Empty objects for the information we want to store
# The PurePublication and PureAuthor objects have a method Store which writes the data to the database

class PureElectronicVersion():
    def __init__(self):
        self.id = u''
        self.type = u''
        self.access_type = u''
        self.version_type = u''
        self.visible_on_portal_date = u''
        self.file_creator = u''
        self.file_created = u''
        self.file_title = u''
        self.file_urls = u''
        self.file_total = None
        self.doi = u''
        self.link = u''
        self.change_fields = ['access_type', 'version_type', 'visible_on_portal_date', 'file_creator', 'file_created',
                              'file_title', 'file_urls', 'file_total', 'doi', 'link'] # only update the db-record if one of these entries was changed


class PureJournal():
    def __init__(self):
        self.uuid = u''
        self.title = u''
        self.issn = u''
        self.change_fields = ['title', 'issn'] # only update the db-record if one of these entries was changed


class PureAuthor():
    def __init__(self):
        self.uuid = u''
        self.firstname = u''
        self.lastname = u''
        self.orcid = u''
        self.scopusid = u''
        self.digitalid = u''
        self.researcherid = u''
        self.isni = u''
        self.change_fields = ['firstname', 'lastname', 'orcid', 'scopusid', 'digitalid', 'researcherid', 'isni'] # only update the db-record if one of these entries was changed

    @staticmethod
    def setDeleted(uuid):
        s = db.Session()
        q = s.query(db.Author).filter(db.Author.uuid == uuid)
        if q.count() == 1:
            aut = q.first()
            aut.deleted = True
            s.add(aut)
            s.commit()
            s.close()

    @staticmethod
    def _changed(obj, db_record):
        for f in obj.change_fields:
            if getattr(db_record, f, '') != getattr(obj, f, ''):
                print('change found in %s in %s: %s to %s' % (
                getattr(db_record, 'uuid', ''), f, getattr(db_record, f, ''), getattr(obj, f, '')))
                return True
        return False

    def store(self):
        s = db.Session()
        global_change = False

        q = s.query(db.Author).filter(db.Author.uuid == self.uuid)
        if q.count() == 0:
            aut = db.Author(
                uuid=self.uuid,
                firstname=self.firstname,
                lastname=self.lastname,
                orcid=self.orcid,
                scopusid=self.scopusid,
                digitalid=self.digitalid,
                researcherid = self.researcherid,
                isni = self.isni
            )
            change = True
        else:
            aut = q.first()
            change = self._changed(self, aut)
            aut.firstname = self.firstname
            aut.lastname = self.lastname
            aut.orcid = self.orcid
            aut.scopusid = self.scopusid
            aut.digitalid = self.digitalid
            aut.researcherid = self.researcherid
            aut.isni = self.isni
            # Do not change the author table when adding publications
        if change:
            global_change = True
            s.add(aut)
        s.commit()
        s.close()
        return global_change


class PureOrganization():
    def __init__(self):
        self.uuid = u''
        self.name = u''
        self.change_fields = ['name'] # only update the db-record if one of these entries was changed


class PurePublication():
    def __init__(self):
        self.uuid = u''
        self.pub_date = None
        self.epub_date = None
        self.title = u''
        self.subtitle = u''
        self.pubtype = u''
        self.category = u''
        self.peer_review = u''
        self.oa_permission = u''
        self.doi = u''
        self.pmid = u''
        self.wos_accnum = u''
        self.scopusid = u''
        self.volume = u''
        self.issue = u''
        self.article_number = u''
        self.pages = u''
        self.ISBN = u''
        self.confidential = u''
        self.visibility = u''
        self.workflow = u''
        self.electronic_versions = [] # List of PureElectronicVersion
        self.journal = PureJournal
        self.authors = [] # List of PureAuthor
        self.organisations = [] # List of PureOrganisation
        self.change_fields = ['pub_date', 'epub_date', 'title', 'subtitle', 'pubtype', 'category', 'peer_review',
                              'oa_permission', 'doi', 'pmid', 'wos_accnum', 'scopusid', 'volume', 'issue',
                              'article_number',
                              'pages', 'ISBN', 'confidential', 'visibility', 'workflow'] # only update the db-record if one of these entries was changed

    @staticmethod
    def _changed(obj, db_record):
        for f in obj.change_fields:
            if getattr(db_record, f, '') != getattr(obj, f, ''):
                return True
        return False

    @staticmethod
    def setDeleted(uuid):
        s = db.Session()
        q = s.query(db.Publication).filter(db.Publication.uuid == uuid)
        if q.count() == 1:
            pub = q.first()
            pub.deleted = True
            s.add(pub)
            s.commit()
            s.close()

    def store(self):
        s = db.Session()
        global_change = False
        # Insert if exists update otherwise
        q = s.query(db.Journal).filter(db.Journal.uuid == self.journal.uuid)
        if q.count() == 0:
            jrn = db.Journal(
                uuid=self.journal.uuid,
                title=self.journal.title,
                issn=self.journal.issn
            )
            change = True
            global_change = True
        else:
            jrn = q.one()
            change = self._changed(self.journal, jrn)
            jrn.title = self.journal.title
            jrn.issn = self.journal.issn
        if change:
            s.add(jrn)
            s.flush()

        q = s.query(db.Publication).filter(db.Publication.uuid == self.uuid)
        if q.count() == 0:
            pub = db.Publication(
                uuid=self.uuid,
                pub_date=self.pub_date,
                epub_date=self.epub_date,
                title=self.title,
                subtitle=self.subtitle,
                pubtype=self.pubtype,
                category=self.category,
                peer_review=self.peer_review,
                oa_permission=self.oa_permission,
                doi=self.doi,
                pmid=self.pmid,
                wos_accnum=self.wos_accnum,
                scopusid=self.scopusid,
                volume=self.volume,
                issue=self.issue,
                article_number=self.article_number,
                journal=jrn,
                pages=self.pages,
                ISBN=self.ISBN,
                confidential=self.confidential,
                visibility=self.visibility,
                workflow=self.workflow,
                deleted=False
            )
            change = True
        else:
            pub = q.first()
            change = self._changed(self, pub)
            pub.pub_date = self.pub_date
            pub.epub_date = self.epub_date
            pub.title = self.title
            pub.subtitle = self.subtitle
            pub.pubtype = self.pubtype
            pub.category = self.category
            pub.peer_review = self.peer_review
            pub.oa_permission = self.oa_permission
            pub.doi = self.doi
            pub.pmid = self.pmid
            pub.wos_accnum = self.wos_accnum
            pub.scopusid = self.scopusid
            pub.volume = self.volume
            pub.issue = self.issue
            pub.article_number = self.article_number
            pub.journal = jrn
            pub.pages = self.pages
            pub.ISBN = self.ISBN
            pub.confidential = self.confidential
            pub.visibility = self.visibility
            pub.workflow = self.workflow
        if change:
            s.add(pub)
            global_change = True
            try:
                s.flush()
            except Exception as e:
                print(e)
                return False

        current_ev_id_list = []
        for electronic_version in self.electronic_versions:
            # Insert if exists update otherwise
            new = False
            q = s.query(db.ElectronicVersion).filter(db.ElectronicVersion.id == electronic_version.id)
            if q.count() == 0:
                ev = db.ElectronicVersion(
                    id=electronic_version.id,
                    type=electronic_version.type,
                    access_type=electronic_version.access_type,
                    version_type=electronic_version.version_type,
                    visible_on_portal_date=electronic_version.visible_on_portal_date,
                    file_creator=electronic_version.file_creator,
                    file_created=electronic_version.file_created,
                    file_title=electronic_version.file_title,
                    file_urls=electronic_version.file_urls,
                    file_total=electronic_version.file_total,
                    doi=electronic_version.doi,
                    link=electronic_version.link
                )
                change = True
                new = True
            else:
                # print('**** Duplicate electronic version %s in publication %s' % (electronic_version.id, self.uuid))
                ev = q.first()
                change = self._changed(electronic_version, ev)
                ev.id = electronic_version.id,
                ev.type = electronic_version.type,
                ev.access_type = electronic_version.access_type,
                ev.version_type = electronic_version.version_type,
                ev.visible_on_portal_date = electronic_version.visible_on_portal_date,
                ev.file_creator = electronic_version.file_creator,
                ev.file_created = electronic_version.file_created,
                ev.file_title = electronic_version.file_title,
                ev.file_urls = electronic_version.file_urls,
                ev.file_total = electronic_version.file_total,
                ev.doi = electronic_version.doi
                ev.link = electronic_version.link
            current_ev_id_list.append(electronic_version.id)
            if change:
                s.add(ev)
            if pub not in ev.publications:
                ev.publications.append(pub)
                global_change = True

        s.flush()

        # remove deleted versions
        for db_ev in pub.electronicversions:
            if db_ev.id not in current_ev_id_list:
                print('**** Remove electronic version %s in publication %s' % (db_ev.id, self.uuid))
                q = s.query(db.ElectronicVersion).filter(db.ElectronicVersion.id == db_ev.id)
                pub.electronicversions.remove(q.first())
                q.delete()
                global_change = True

        current_author_uuid_list = []
        for author in self.authors:
            current_author_uuid_list.append(author.uuid)
            # Insert if exists update otherwise
            q = s.query(db.Author).filter(db.Author.uuid == author.uuid)
            if q.count() == 0:
                aut = db.Author(
                    uuid=author.uuid,
                    firstname=author.firstname,
                    lastname=author.lastname,
                )
                change = True
            else:
                aut = q.first()
                # Do not change the author table when adding publications
                change = False
            if change:
                s.add(aut)
            if pub not in aut.publications:
                aut.publications.append(pub)
                global_change = True
        s.flush()

        # remove deleted author/publication combinations
        for author in pub.authors:
            if author.uuid not in current_author_uuid_list:
                print('**** Remove author %s in publication %s' % (author.uuid, self.uuid))
                q = s.query(db.Author).filter(db.Author.uuid == author.uuid)
                pub.authors.remove(q.first())
                global_change = True

        current_org_uuid_list = []
        for organization in self.organisations:
            current_org_uuid_list.append(organization.uuid)
            # Voeg nieuwe org toe
            q = s.query(db.Organization).filter(db.Organization.uuid == organization.uuid)
            if q.count() == 0:
                org = db.Organization(
                    uuid=organization.uuid,
                    name=organization.name
                )
                change = True
                global_change = True
            else:
                org = q.first()
                change = self._changed(organization, org)
                org.name = organization.name
            if change:
                s.add(org)
            if pub not in org.publications:
                org.publications.append(pub)
                global_change = True

        # remove deleted organization/publication combinations
        for org in pub.organizations:
            if org.uuid not in current_org_uuid_list:
                print('**** Remove organization %s in publication %s' % (org.uuid, self.uuid))
                q = s.query(db.Organization).filter(db.Organization.uuid == org.uuid)
                pub.organizations.remove(q.first())
                global_change = True

        s.commit()
        s.close()
        return global_change
