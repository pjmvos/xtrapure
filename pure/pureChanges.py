import pure.ResearchOutputsAPI as ro
import pure.PersonsAPI as pa
import pure.ChangesAPI as ca
import db.schema as db
from datetime import datetime


def get_pure_changes():
    start = datetime.now()
    job_name = 'get pure changes'
    s = db.Session()
    start_total = s.query(db.Publication).filter(db.Publication.deleted == False).count()

    # get the last id from the previous run
    lastjob = s.query(db.Jobs).filter(db.Jobs.action == job_name).order_by(db.Jobs.job_date.desc()).limit(1).first()
    id = lastjob.last_id

    # just close the session in case the script runs for days
    s.close()
    id=''
    # create a list of relevant changes
    if (id is None) or (id == ''):
        date = lastjob.job_date.date().strftime('%Y-%m-%d')
        list, last_id = ca.get_changes(date_or_id=date)
    else:
        list, last_id = ca.get_changes(date_or_id=id)

    # process all research output changes and deletes
    db_changes = ro.get_by_uuid_list(list['uuid'])
    db_deletes = ro.set_deletes(list['uuid_deleted'])

    # process all author changes and deletes
    db_author_changes = pa.get_by_uuid_list(list['person'])
    db_author_deletes = pa.set_deletes(list['person_deleted'])

    # Update the jobs table
    s = db.Session()
    end_total = s.query(db.Publication).filter(db.Publication.deleted == False).count()
    end = datetime.now()
    job = db.Jobs(
        action=job_name,
        changes=len(list['uuid']),
        db_changes=db_changes,
        deletes=db_deletes,
        author_changes=db_author_changes,
        author_deletes=db_author_deletes,
        first_id=id,
        last_id=last_id,
        start_total=start_total,
        end_total=end_total,
        duration=(end - start).seconds
    )
    s.add(job)
    s.commit()
    s.close()
