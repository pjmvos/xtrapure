from conf.config import PURE_APIKEY, PURE_CHANGES_URL, PURE_USERNAME, PURE_PASSWORD
import requests, requests_cache
import pprint

from lxml import etree as ET
import datetime
import time


requests_cache.install_cache(cache_name='cache/pure_requests_cache', allowable_methods=('GET', 'POST'))


def do_request(date_or_id, repeat=0):
    '''
    Request a single page from the Pure Changes API

    There have been some problems connecting to the changes API, hence the repeat

    :param date_or_id:
    :param repeat:
    :return: the request response
    '''
    headers = {'Accept': 'application/xml'}
    payload = {
        'apiKey': PURE_APIKEY,
    }
    try:
        res = requests.get('%s/%s' % (PURE_CHANGES_URL, date_or_id), headers=headers, params=payload) # ,
        #                   auth=(PURE_USERNAME, PURE_PASSWORD))
    except:
        # nog een keer?
        print('failed getting change %s' % date_or_id)
        if repeat < 5:
            time.sleep(60)
            do_request(date_or_id, repeat=repeat + 1)
        else:
            return False
    #print('yasin request result', res, res.url)
    return res


def _retrieve_part(date_or_id, list):
    '''
    Retrieve and parse a page of the changes API output and append them to the list

    We are only interested in "familySystemName" ResearchOutput or Person

    :param date_or_id:
    :param list:
    :return:
                more: True for more changes
                last_id: id of last change on the page
                list: the appended list
                cnt: total number of changes on the page
    '''
    res = do_request(date_or_id)
    r = ET.fromstring(res.content.decode('latin-1'))
    more = True if r.find('./moreChanges').text == 'true' else False
    last_id = r.find('./resumptionToken').text
    changes = r.xpath('./items/contentChange')
    cnt = 0
    for c in changes:
        cnt = cnt + 1
        uuid = str(c.find('./uuid').text)
        changeType = c.find('./changeType').text
        sysName = c.find('./familySystemName').text
        if sysName == 'ResearchOutput':
            if (changeType == 'DELETE'):
                if uuid not in list['uuid_deleted']:
                    list['uuid_deleted'].append(uuid)
            elif uuid not in list['uuid']:
                list['uuid'].append(uuid)
        elif sysName == 'Person':
            if (changeType == 'DELETE'):
                if uuid not in list['person_deleted']:
                    list['person_deleted'].append(uuid)
            elif uuid not in list['person']:
                list['person'].append(uuid)
    return more, last_id, list, cnt


def retrieve(date_or_id, list):
    '''
    keep on retrieving a page of changes using _retrieve_part and appending to the list until there are no more changes

    :param date_or_id:
    :param list:
    :return:    list, pages (nr of pages retrieved), last (last id nr), all_cnt (total number of all changes)
    '''
    more = True
    last_id = date_or_id
    pages = 0
    all_cnt = 0
    while more:
        # Python doesn't really like recursion
        last = last_id  # take the first to last page to resume on the next day
        #print('yasin: entering computation for variable more')
        more, last_id, list, cnt = _retrieve_part(last_id, list)
        all_cnt = all_cnt + cnt
        pages = pages + 1
    return list, pages, last, all_cnt


def get_changes(date_or_id):
    '''
    Initialize the list and fire retrieve starting from date_or_id

    :param date_or_id:
    :return: list, last_id
    '''
    print('start querying for changes')
    print('retrieve changes from id or date %s ' % date_or_id)
    list = {}
    list['uuid'] = []
    list['uuid_deleted'] = []
    list['person'] = []
    list['person_deleted'] = []
    list, pages, last_id, all_cnt = retrieve(date_or_id=date_or_id, list=list)
    print('found %s changed and %s deleted ResearchOutputs in %s requests. Total changes %s. Last change: %s' % ( len(list['uuid']), len(list['uuid_deleted']), pages, all_cnt, last_id))
    print('found %s changed and %s deleted Persons' % (len(list['person']), len(list['person_deleted'])))
    return list, last_id
