import re


def parse_doi(doi_string):
    # https://stackoverflow.com/questions/27910/finding-a-doi-in-a-document-or-page#29764
    m = re.search(r'10[.][0-9]{4,}(?:[.][0-9]+)*/(?:(?!["&\'<>])\S)+', doi_string)
    if m:
        return m.group(0).strip()
    else:
        return ''
