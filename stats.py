import db.schema as db
from datetime import datetime


# total nr of docs
def get_total_pure_publications():
    s = db.Session()
    count = s.query(db.Publication).filter(db.Publication.deleted == False).count()
    s.close()
    return count


# of docs with pdf
def get_pubs_with_pdf():
    s = db.Session()
    q = s.query(db.Publication.uuid) \
        .filter(db.Publication.deleted == False) \
        .filter(db.ElectronicVersion.file_total > 0) \
        .filter(db.ElectronicVersionHasPublication.electronicversion_id == db.ElectronicVersion.id) \
        .filter(db.Publication.uuid == db.ElectronicVersionHasPublication.publication_uuid) \
        .distinct()
    count = q.count()
    s.close()
    return count


# of OA docs
def get_oa():
    s = db.Session()
    count = s.query(db.Publication) \
        .filter((db.Publication.deleted == False) &
                (db.Publication.oa_permission == '/dk/atira/pure/researchoutput/openaccesspermission/open')) \
        .count()
    s.close()
    return count


# of docs with scopus id
def get_pubs_with_scopus():
    s = db.Session()
    count = s.query(db.Publication).filter((db.Publication.deleted == False) & (db.Publication.scopusid != '')).count()
    s.close()
    return count


# of docs with doi
def get_pubs_with_doi():
    s = db.Session()
    q = s.query(db.Publication.uuid) \
        .filter(db.Publication.deleted == False) \
        .filter(db.ElectronicVersion.type == 'wsElectronicVersionDoiAssociation') \
        .filter(db.ElectronicVersionHasPublication.electronicversion_id == db.ElectronicVersion.id) \
        .filter(db.Publication.uuid == db.ElectronicVersionHasPublication.publication_uuid).distinct()
    count = q.count()
    s.close()
    return count


def get_pubs_with_unpaywall_oa():
    s = db.Session()
    q = s.query(db.Publication.uuid) \
        .filter(db.Publication.deleted == False) \
        .filter(db.UnPaywall.is_oa) \
        .filter(db.UnPaywallHasPublication.doi == db.UnPaywall.doi) \
        .filter(db.Publication.uuid == db.UnPaywallHasPublication.publication_uuid).distinct()
    #print(q)
    count = q.count()
    s.close()
    return count

def get_person_with_id():
    s = db.Session()
    scopus = s.query(db.Author).filter((db.Author.deleted == False) & (db.Author.scopusid != '')).count()
    orcid = s.query(db.Author).filter((db.Author.deleted == False) & (db.Author.orcid != '')).count()
    digital = s.query(db.Author).filter((db.Author.deleted == False) & (db.Author.digitalid != '')).count()
    researcherid = s.query(db.Author).filter((db.Author.deleted == False) & (db.Author.researcherid != '')).count()
    total = s.query(db.Author).filter(db.Author.deleted == False).count()
    s.close()
    return total, scopus, orcid, digital, researcherid

def do_get():
    total = get_total_pure_publications()
    with_pdf = get_pubs_with_pdf()
    with_doi = get_pubs_with_doi()
    with_scopus = get_pubs_with_scopus()
    with_up_is_oa = get_pubs_with_unpaywall_oa()
    person_total, person_with_scopus, person_with_orcid, person_with_digital, person_with_researcherid=get_person_with_id()

    oa = get_oa()

    stat = db.Stats(
        total=total,
        with_pdf=with_pdf,
        with_doi=with_doi,
        with_scopus=with_scopus,
        with_up_is_oa=with_up_is_oa,
        oa=oa,
        person_total=person_total,
        person_with_scopus=person_with_scopus,
        person_with_orcid=person_with_orcid,
        person_with_digital=person_with_digital,
        person_with_researcherid=person_with_researcherid
    )
    s = db.Session()
    s.add(stat)
    s.commit()
    s.close()


def get_stats():
    # store some totals in the stats table
    start = datetime.now()
    job_name = 'get stats'
    do_get()
    end = datetime.now()
    job = db.Jobs(
        action=job_name,
        duration=(end - start).seconds
    )
    s = db.Session()
    s.add(job)
    s.commit()
    s.close()
